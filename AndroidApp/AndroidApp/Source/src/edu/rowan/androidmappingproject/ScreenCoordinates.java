package edu.rowan.androidmappingproject;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 4/16/13
 * 
 * Modified 3/12/2015 Josh Klodnicki
 *      - Made bottomLeft and topRight work with rotation
 *      - Added center
 *      - Made attributes public, got rid of set/get methods ("struct" format)
 *      
 * Modified 4/21/2015 Josh Klodnicki
 *      - Moved to FirstActivity, this is now unused
 */
public class ScreenCoordinates {
    public LatLng topLeft, topRight, bottomLeft, bottomRight, center;
}
