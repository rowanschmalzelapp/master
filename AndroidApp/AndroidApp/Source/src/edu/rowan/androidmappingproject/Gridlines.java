/**
 * 
 */
package edu.rowan.androidmappingproject;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import edu.rowan.androidmappingproject.FirstActivity.ScreenCoordinates;

/**
 * Generates PolylineOptions for drawing Lat/Lng Gridlines within the current screen location
 * 
 * @author Josh Klodnicki
 * @date   3/12/2015
 * 
 * @param coords Screen coordinates within which to draw gridlines
 * 
 * @todo Draw actual Lat/Lng gridlines
 * @todo Adjust Lat/Lng resolution based on zoom level
 * @todo Draw gridlines inside this class?
 */
public class Gridlines {
	
	public static PolylineOptions generateGridlines() {
		
		LatLng topLeft     = ScreenCoordinates.topLeft;
		LatLng topRight    = ScreenCoordinates.topRight;
		LatLng bottomLeft  = ScreenCoordinates.bottomLeft;
		LatLng bottomRight = ScreenCoordinates.bottomRight;
		
		// Find the corner farthest North, South, West, and East
		double minLat = min4(topLeft.latitude,  topRight.latitude,  bottomLeft.latitude,  bottomRight.latitude);
		double maxLat = max4(topLeft.latitude,  topRight.latitude,  bottomLeft.latitude,  bottomRight.latitude);
		double minLng = min4(topLeft.longitude, topRight.longitude, bottomLeft.longitude, bottomRight.longitude);
		double maxLng = max4(topLeft.longitude, topRight.longitude, bottomLeft.longitude, bottomRight.longitude);
		
		// Find the corners of the box around the screen (aligned with North)
		LatLng NW = new LatLng(maxLat, minLng);
		LatLng NE = new LatLng(maxLat, maxLng);
		LatLng SW = new LatLng(minLat, minLng);
		LatLng SE = new LatLng(minLat, maxLng);
		
		
        PolylineOptions result = new PolylineOptions()
        	.add(NW)
        	.add(NE)
        	.add(SE)
        	.add(SW)
        	.add(NW)
        	.add(SE)
        	.add(NE)
        	.add(SW);
        
        return result; // A PolylineOptions object
	}
	
	/**
	 * Returns the minimum of 4 arguments
	 * @author Josh Klodnicki
	 */
	private static double min4(double a, double b, double c, double d) {
		return Math.min(Math.min(a, b), Math.min(c, d));
	}

	/**
	 * Returns the maximum of 4 arguments
	 * @author Josh Klodnicki
	 */
	private static double max4(double a, double b, double c, double d) {
		return Math.max(Math.max(a, b), Math.max(c, d));
	}
}
