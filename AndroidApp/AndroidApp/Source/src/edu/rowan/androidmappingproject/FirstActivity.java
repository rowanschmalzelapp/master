package edu.rowan.androidmappingproject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.*;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import android_OpenMap_Framework.common.LatLonPoint;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.*;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import static android.graphics.Bitmap.Config;
import static android.graphics.Bitmap.createBitmap;
import static edu.rowan.androidmappingproject.FirstActivity.SYMBOLDICT.FriendlyAirSpace;
import static java.lang.Double.parseDouble;

/**
 * This is the first class that will run when the app starts up.
 * Initializes the map, sets main layout, checks to see if play services are correctly installed,
 * and initializes the GPS thread.
 *
 * @author Justin Reda
 * @version 1.0.0
 */
public class FirstActivity extends Activity {
	
	// "Struct" storing current screen position
	public static class ScreenCoordinates{
		public static LatLng topLeft, topRight, bottomLeft, bottomRight, center;
	}

    //keep track of which symbol is being selected
    public static final SYMBOLDICT[] currentChoice = new SYMBOLDICT[2];
    public static Polyline distLine = null;
    //variable of the button with the current marker in the sidebar
    public static ImageButton ibSidebarMain = null;

    //variable to control which type of coordinate is present
    //public int previousCoordsType = 3;

    //dont remember what these are for
    // public LatLonPoint postConversionLLpt = new LatLonPoint(0, 0);
    //public UTMPoint convertedUTMpoint = new UTMPoint();

    //variable to see if long click is in use or not
    //public boolean LongClickOn = true;
    public static Polyline gridlines = null;
    //variable to control the minimap bounding box
    public static Polyline miniMapBoundingBox = null;
    //variables for the where ive been polyline
    public static Polyline whereIveBeenPL = null;
    public static MyMarker[] myMarkerStore = new MyMarker[100000];
    //variable to check if sidebar is open or not.
    public boolean sidebarOpen = true;
    public boolean gpsON = false;
    public double userLatitude = 0;
    public double userLongitude = 0;
    public double repositionLat = 0;
    public double repositionLon = 0;
    public double cameraAlt = 0;
    //variables for the calculate distance command
    public boolean distanceMarkersPresent = false;
    public boolean whereIveBeenOn = false;
    public LatLng whereIveBeenLastPoint = null;
    public LatLng whereIveBeenNewPoint = null;
    public double whereIveBeenDistance = 0;
    //variables for the flashlight to work
    public boolean lightOn = false;
    public Flashlight flashlight;
    public String[] ssidList;
    //for calculating distance between coordinates
    public boolean distanceBetweenCoordsActive = false;
    public int distanceBetweenCoordsNum = 0;
    public LatLng distanceBetweenCoordsPt1;
    public LatLng distanceBetweenCoordsPt2;
    //debug variables
    Boolean D = true;
    String debugTag = "FirstActivity";
    Boolean T = false;    //True for debug toasts *This will slow down map
    //GPS necessary variables
    FindCurrentGPSLocation findCurrentGPSLocation;
    //variable for the shared preferences of the app
    SharedPreferences sharedPref;
    //WifiFinder Variables
    WifiManager wifiManager;
    WifiScanReceiver wifiReceiver;
    int numResults;
    List<ScanResult> scanResultList;
    MyMarker[] markerContainerOne = new MyMarker[99];
    MyMarker[][] markerContainersArray = new MyMarker[99][99];
    int nextContainer = 9999;
    int pixelDistanceToMerge = 30;
    MyAsyncTask myAsyncTask;
    boolean isGridManagerRunning = false;
    private Vector<Point> myMarkerPixelCoord;

    /**
     * This function will draw a new marker with the given parameters and update number of markers.
     *
     * @param drawable      provide R.drawable.resource_name
     * @param title         String title
     * @param snippit       String snippit
     * @param latLng        LatLng point for the coordinates, the default data type coming off map
     * @param markerStoreID int to set the new marker to a specific position in the marker store (dangerous)
     * @since 1.0.0
     */
    @SuppressWarnings("unused")
    public static void makeNewMarker(int drawable, String title, String snippit, LatLng latLng, int markerStoreID) {
        myMarkerStore[markerStoreID] = new MyMarker();
        myMarkerStore[markerStoreID].setIcon(drawable);
        myMarkerStore[markerStoreID].setSnippit(snippit);
        myMarkerStore[markerStoreID].setTitle(title);
        myMarkerStore[markerStoreID].setLatLng(latLng);
        myMarkerStore[markerStoreID].setSnippitID(markerStoreID);

        GoogleMap googleMap = Global.gmap;

        Marker testMarker = googleMap.addMarker(new MarkerOptions()
                .position(myMarkerStore[markerStoreID].getLatLng())
                .title(myMarkerStore[markerStoreID].getTitle())
                .snippet(myMarkerStore[markerStoreID].getSnippit())
                .icon(BitmapDescriptorFactory.fromResource(myMarkerStore[markerStoreID].getIcon())));
        myMarkerStore[markerStoreID].setId(markerStoreID);
        myMarkerStore[markerStoreID].setMarker(testMarker);
        Global.numMarkers++;
        //Log.d("First Activity", "Num Markers = " + (Global.numMarkers - 1));


    }

    /**
     * This method writes data to the internal storage directory of the app given appropriate input parameters, filename and data.
     *
     * @param FILENAME String of the filename to write, Ex) "file.txt"
     * @param value    String of the physical data to put into the new file.
     * @since 1.0.0
     */
    public static void writeIntoInternalStorage(String FILENAME, String value) {
        FileOutputStream fos;
        try {
            fos = Global.context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(value.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This will return a file from the internal storage directory of the app.
     *
     * @param FILENAME String of the filename to be retrieved
     * @return a long string of the data from the retrieved file.
     * @throws IOException if the file is not found an exception will be thrown so the app doesn't explode
     * @since 1.0.0
     */
    public static String returnInternalStorageInfo(String FILENAME) throws IOException {

        FileInputStream fis;
        String data = null;
        if (checkForFile(FILENAME)) {
            try {
                fis = Global.context.openFileInput(FILENAME);
                InputStreamReader in = new InputStreamReader(fis);
                BufferedReader br = new BufferedReader(in);
                data = br.readLine();
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            writeIntoInternalStorage(Global.context.getString(R.string.latFN), Double.toString(0));
            writeIntoInternalStorage(Global.context.getString(R.string.lonFN), Double.toString(0));
            writeIntoInternalStorage(Global.context.getString(R.string.altFN), Double.toString(0));
        }
        return data;
    }

    /**
     * This method will check to see if a file exists, given its full file path
     *
     * @param fileName String of the path to file and name of file to check for.
     * @return boolean, true if file exists, false otherwise
     * @since 1.0.0
     */
    private static boolean checkForFile(String fileName) {
        File file = Global.context.getFileStreamPath(fileName);
        return file.exists();
    }

    private static boolean checkForDownloadsFiles(String filename) {
        File[] downloadFiles = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).listFiles();
        for (int i = 0; i < downloadFiles.length; i++) {
            if (downloadFiles[i].getName().matches(filename)) return true;
        }
        return false;
    }

/*    private void addToContainer(Marker markerToAdd){
        String markID = markerToAdd.getId();
        int markerID;
        markerID = Integer.parseInt(markID.substring(1));
        multipleMarkerContainer.addMarkerToArray(markerContainerOne,myMarkerStore[markerID]);
    }*/

    /**
     * Called when the activity is first created.
     * This will set the layout to main.xml. Call the checkForPlayServices() method.
     * If Play Services are found will setup the map fragment under tag "googleMap" for modifications.
     *
     * @since 1.0.0
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //lock in landscape and set view
        this.setRequestedOrientation(0);
        setContentView(R.layout.main);

        //Enable the ActionBar
        ActionBar actionBar = this.getActionBar();
        actionBar.show();

        //Set up prefs, global context var, and symbol image button on sidebar
        Global.context = getApplicationContext();
        sharedPref = PreferenceManager.getDefaultSharedPreferences(Global.context);
        ibSidebarMain = (ImageButton) findViewById(R.id.sidebarSymbolDisplayIB);

        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "wifi is disabled..making it enabled", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }

        //Set up default settings files so the app doesn't explode
        if (!checkForFile(getString(R.string.offlineMapPathFN))) {
            writeIntoInternalStorage(getString(R.string.offlineMapPathFN), "sdcard1/cyclemap/");
        }
        if (!checkForFile(getString(R.string.offlineMapTilePrefixFN))) {
            writeIntoInternalStorage(getString(R.string.offlineMapTilePrefixFN), "oc_");
        }
        if (!checkForFile(getString(R.string.defaultSymbolFN))) {
            writeIntoInternalStorage(getString(R.string.defaultSymbolFN), "FriendlyGround");
        }

        //setup default symbol
        String storedChoice;
        try {
            storedChoice = returnInternalStorageInfo(getString(R.string.defaultSymbolFN));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            storedChoice = SYMBOLDICT.HostileGround.toString();
            Log.e("FirstActivity", " read symbol file error");
        }
        if (D) Log.d(debugTag, "Starting App Symbol Choice= " + storedChoice);

        //set default symbol to actually work
        currentChoice[0] = returnSymbolFromString(storedChoice);

        //make sure the maps will work at all
        if (checkForPlayServices()) {
            //Play services found begin using map functionality

            //Starts up a new thread which is updating GPS location.
            findCurrentGPSLocation = new FindCurrentGPSLocation();
            findCurrentGPSLocation.run();
            gpsON = true;

            GoogleMap googleMap = getTheDamnMap();
            //All map stuff is added to or referenced from "googleMap"
            Global.gmap = googleMap;

            //Enable compass and zoom controls
            turnOnMapControls();

            //set the custom marker info boxes
            googleMap.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater()));

            //Start the screen centered at last user position
            //if not previous position is found will default to Rowan Hall
            try {
                restorePreviousPosition();
            } catch (IOException e) {
                e.printStackTrace();
            }
            LatLng lastLatLong = new LatLng(Global.CURRENT_LAT, Global.CURRENT_LON);
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lastLatLong, 17));

            //initialize markers
            initializeMarkers();

            //enable a default option so the sidebar isn't blank
            enableMapTypeOptions();

        } else {
            //Play services not found, nothing is gonna work!
            Toast.makeText(FirstActivity.this, "Play Services Not Functioning", Toast.LENGTH_LONG).show();
        }
        
    }

    /**
     * Utility function because I got tired of writing these 3 lines in every function and it was late.
     *
     * @return the GoogleMap from FragmentManager.MapFragment.getMap()
     * @since 1.0.0
     */
    public GoogleMap getTheDamnMap() {
        FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment) fragmentManager.findFragmentById(R.id.map);
        return mapFragment.getMap();
    }

    /**
     * This function will return a SYMBOL given an input string.
     * Use this functionality in order to restore default marker from its name only.
     *
     * @param inputString name of the marker, corresponds to one of the Strings in SYMBOLDICT Enum.
     * @return SYMBOLDICT Enum which can be used to draw markers.
     * @since 1.0.0
     */
    public SYMBOLDICT returnSymbolFromString(String inputString) {
        SYMBOLDICT symbol;

        if (inputString.matches(SYMBOLDICT.FriendlyAirSpace.toString())) symbol = SYMBOLDICT.FriendlyAirSpace;
        else if (inputString.matches(SYMBOLDICT.FriendlyGround.toString())) symbol = SYMBOLDICT.FriendlyGround;
        else if (inputString.matches(SYMBOLDICT.FriendlySeaSurface.toString())) symbol = SYMBOLDICT.FriendlySeaSurface;
        else if (inputString.matches(SYMBOLDICT.FriendlySubsurface.toString())) symbol = SYMBOLDICT.FriendlySubsurface;

        else if (inputString.matches(SYMBOLDICT.HostileAirSpace.toString())) symbol = SYMBOLDICT.HostileAirSpace;
        else if (inputString.matches(SYMBOLDICT.HostileGround.toString())) symbol = SYMBOLDICT.HostileGround;
        else if (inputString.matches(SYMBOLDICT.HostileSeaSurface.toString())) symbol = SYMBOLDICT.HostileSeaSurface;
        else if (inputString.matches(SYMBOLDICT.HostileSubsurface.toString())) symbol = SYMBOLDICT.HostileSubsurface;

        else if (inputString.matches(SYMBOLDICT.NeutralAirSpace.toString())) symbol = SYMBOLDICT.NeutralAirSpace;
        else if (inputString.matches(SYMBOLDICT.NeutralGround.toString())) symbol = SYMBOLDICT.NeutralGround;
        else if (inputString.matches(SYMBOLDICT.NeutralSeaSurface.toString())) symbol = SYMBOLDICT.NeutralSeaSurface;
        else if (inputString.matches(SYMBOLDICT.NeutralSubsurface.toString())) symbol = SYMBOLDICT.NeutralSubsurface;

        else if (inputString.matches(SYMBOLDICT.UnknownAirSpace.toString())) symbol = SYMBOLDICT.UnknownAirSpace;
        else if (inputString.matches(SYMBOLDICT.UnknownGround.toString())) symbol = SYMBOLDICT.UnknownGround;
        else if (inputString.matches(SYMBOLDICT.UnknownSeaSurface.toString())) symbol = SYMBOLDICT.UnknownSeaSurface;
        else if (inputString.matches(SYMBOLDICT.UnknownSubsurface.toString())) symbol = SYMBOLDICT.UnknownSubsurface;

        else symbol = SYMBOLDICT.FriendlyGround;

        return symbol;
    }

/*
    public void changeDeletableMarker(final Marker marker) {
        Button removeBut = (Button) findViewById(R.id.removeMarkerButton);
        removeBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String markID = marker.getId();
                int markerID;
                try {
                    if (marker.getTitle().contains("My Position")) {
                        deleteMyPositionMarker();
                    } else {
                        markerID = Integer.parseInt(markID.substring(1));
                        FirstActivity.myMarkerStore[markerID] = null;
                        marker.remove();
                    }
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
            }
        });
    }*/

    /**
     * This function will search the marker store for the input title. The marker itself will be returned.
     *
     * @param searchTitle The string to search for as the title field
     * @return the marker that corresponds to the entered title string.
     * @since 1.0.0
     */
    @SuppressWarnings("unused")
    private Marker searchForMarkerByTitle(String searchTitle) {
        Marker marker = null;
        int i = 0;

        do {
            if (myMarkerStore[i].getTitle().contentEquals(searchTitle)) return myMarkerStore[i].getMarker();
            else i++;

        } while (i < Global.numMarkers);
        return marker;
    }

    /**
     * This function will delete the marker at myMarkerStore[Global.showMyPOSMarkerID]. this SHOULD be the show my location
     * marker that was put down by the user. we use this function so there is only one "my location" at a time.
     *
     * @since 1.0.0
     */
    private void deleteMyPositionMarker() {
        if (myMarkerStore[Global.showMyPOSMarkerID] != null) {
            myMarkerStore[Global.showMyPOSMarkerID].getMarker().remove();
            myMarkerStore[Global.showMyPOSMarkerID] = null;
        }
    }

    /**
     * This function initializes the markers by creating a marker at rowan hall then deleting it.
     * The purpose is to make sure everything is working with the marker system and initialize marker store.
     *
     * @since 1.0.0
     */
    private void initializeMarkers() {
        Global.numMarkers = 0;
        LatLng rowanLatLong = new LatLng(39.71224, -75.1222);
        GoogleMap googleMap = getTheDamnMap();

        myMarkerStore[Global.numMarkers] = new MyMarker();
        myMarkerStore[Global.numMarkers].setIcon(R.drawable.neutral_ground);
        myMarkerStore[Global.numMarkers].setSnippit("Test Icon");
        myMarkerStore[Global.numMarkers].setTitle("TestIcoTitle");
        myMarkerStore[Global.numMarkers].setLatLng(rowanLatLong);

        Marker testMarker = googleMap.addMarker(new MarkerOptions()
                .position(myMarkerStore[Global.numMarkers].getLatLng())
                .title(myMarkerStore[Global.numMarkers].getTitle())
                .snippet(myMarkerStore[Global.numMarkers].getSnippit())
                .icon(BitmapDescriptorFactory.fromResource(myMarkerStore[0].getIcon())));

        myMarkerStore[Global.numMarkers].setId(Global.numMarkers);
        myMarkerStore[Global.numMarkers].setMarker(testMarker);
        Global.numMarkers++;

        //yes just remove it now that its added we will need this as my location placeholder.
        myMarkerStore[0].getMarker().remove();


        Bitmap bitmap;
        bitmap = createBitmap(70, 70, Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.TRANSPARENT);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPaint(paint);

        paint.setColor(Color.BLACK);
        paint.setTextSize(18);

        canvas.drawText("17R", 10, 15, paint);
        GroundOverlayOptions goo = new GroundOverlayOptions();
        goo.position(rowanLatLong, 50);
        goo.image(BitmapDescriptorFactory.fromBitmap(bitmap));

        googleMap.addGroundOverlay(goo);

        //  clusterManager = new ClusterManager<ClusterItem>(getApplicationContext(),googleMap);

    }

    /**
     * This function will draw a new marker with the given parameters and update number of markers.
     *
     * @param drawable provide R.drawable.resource_name
     * @param title    String title
     * @param snippit  String snippit
     * @param latLng   LatLng point for the coordinates, the default data type coming off map
     * @since 1.0.0
     */
    private void makeNewMarker(int drawable, String title, String snippit, LatLng latLng) {
        int markerStoreID = Global.numMarkers;
        myMarkerStore[markerStoreID] = new MyMarker();
        myMarkerStore[markerStoreID].setIcon(drawable);
        myMarkerStore[markerStoreID].setSnippit(snippit);
        myMarkerStore[markerStoreID].setTitle(title);
        myMarkerStore[markerStoreID].setLatLng(latLng);
        myMarkerStore[markerStoreID].setSnippitID(markerStoreID);
        myMarkerStore[markerStoreID].setMarkerStr(currentChoice[0].stringVal);
        GoogleMap googleMap = getTheDamnMap();

        Marker testMarker = googleMap.addMarker(new MarkerOptions()
                .position(myMarkerStore[markerStoreID].getLatLng())
                .title(myMarkerStore[markerStoreID].getTitle())
                .snippet(myMarkerStore[markerStoreID].getSnippit())
                .icon(BitmapDescriptorFactory.fromResource(myMarkerStore[markerStoreID].getIcon())));
        myMarkerStore[markerStoreID].setId(markerStoreID);
        myMarkerStore[markerStoreID].setMarker(testMarker);
        Global.numMarkers++;
        if (D) Log.d(debugTag, "Num Markers = " + (Global.numMarkers - 1));


    }

    private MultipleMarkerContainer makeNewMarkerContainer(LatLng latLng) {

        MultipleMarkerContainer multipleMarkerContainer = new MultipleMarkerContainer();
        multipleMarkerContainer.setMarkerContainerLatLng(latLng);
        multipleMarkerContainer.setMarkerArray(markerContainerOne);
        Marker tempMarker = /*getTheDamnMap().addMarker(new MarkerOptions()
                .position(latLng)
                .title("Marker Container")
                .snippet("Snippit for Container")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.distance_marker_green_pin)));*/
                makeNewMarkerPleaseReturn(R.drawable.distance_marker_green_pin, "Marker Container", "Snippit for Marker Container", latLng);
        multipleMarkerContainer.setThisMarker(tempMarker);
        multipleMarkerContainer.setMarkerContainerSnippit("Snippit for Container");
        multipleMarkerContainer.setMarkerContainerTitle("Marker Container");
        return multipleMarkerContainer;
    }

    private void addToContainer(final Marker markerToAdd) {
        Button addButt = (Button) findViewById(R.id.addToContainerButton);
        addButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String markID = markerToAdd.getId();
                int markerID;
                markerID = Integer.parseInt(markID.substring(1));
                markerContainerOne = Global.testMMC.addMarkerToArray(markerContainerOne, myMarkerStore[markerID]);

            }
        });
    }

    private void dropNewContainer(final LatLng latLng) {
        Button dropButt = (Button) findViewById(R.id.dropMarkerContainerButton);
        nextContainer = 9999;
        dropButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.testMMC = makeNewMarkerContainer(latLng);
            /*    for (int i = 0; i <= 99; i++) {
                    if (Global.multipleMarkerContainers[i] == null) {
                        nextContainer = i;
                        i = 99;
                    }
                }
                Global.multipleMarkerContainers[nextContainer] = Global.testMMC;*/
            }
        });
    }

    /**
     * This function will draw a new marker with the given parameters and update number of markers. will return the new marker object
     *
     * @param drawable provide R.drawable.resource_name
     * @param title    String title
     * @param snippit  String snippit
     * @param latLng   LatLng point for the coordinates, the default data type coming off map
     * @since 1.0.0
     */
    @SuppressWarnings("unused")
    private Marker makeNewMarkerPleaseReturn(int drawable, String title, String snippit, LatLng latLng) {
        int markerStoreID = Global.numMarkers;
        myMarkerStore[markerStoreID] = new MyMarker();
        myMarkerStore[markerStoreID].setIcon(drawable);
        myMarkerStore[markerStoreID].setSnippit(snippit);
        myMarkerStore[markerStoreID].setTitle(title);
        myMarkerStore[markerStoreID].setLatLng(latLng);
        myMarkerStore[markerStoreID].setSnippitID(markerStoreID);

        GoogleMap googleMap = getTheDamnMap();

        Marker testMarker = googleMap.addMarker(new MarkerOptions()
                .position(myMarkerStore[markerStoreID].getLatLng())
                .title(myMarkerStore[markerStoreID].getTitle())
                .snippet(myMarkerStore[markerStoreID].getSnippit())
                .icon(BitmapDescriptorFactory.fromResource(myMarkerStore[markerStoreID].getIcon())));
        myMarkerStore[markerStoreID].setId(markerStoreID);
        myMarkerStore[markerStoreID].setMarker(testMarker);
        Global.numMarkers++;
        if (D) Log.d(debugTag, "Num Markers = " + (Global.numMarkers - 1));

        return testMarker;
    }

    /**
     * This function will draw a new marker with the given parameters and update number of markers. will return the int value of the marker in the marker store.
     *
     * @param drawable provide R.drawable.resource_name
     * @param title    String title
     * @param snippit  String snippit
     * @param latLng   LatLng point for the coordinates, the default data type coming off map
     * @since 1.0.0
     */
    private int makeNewMarkerPleaseReturnInt(int drawable, String title, String snippit, LatLng latLng) {
        int markerStoreID = Global.numMarkers;
        myMarkerStore[markerStoreID] = new MyMarker();
        myMarkerStore[markerStoreID].setIcon(drawable);
        myMarkerStore[markerStoreID].setSnippit(snippit);
        myMarkerStore[markerStoreID].setTitle(title);
        myMarkerStore[markerStoreID].setLatLng(latLng);
        myMarkerStore[markerStoreID].setSnippitID(markerStoreID);

        GoogleMap googleMap = getTheDamnMap();

        Marker testMarker = googleMap.addMarker(new MarkerOptions()
                .position(myMarkerStore[markerStoreID].getLatLng())
                .title(myMarkerStore[markerStoreID].getTitle())
                .snippet(myMarkerStore[markerStoreID].getSnippit())
                .icon(BitmapDescriptorFactory.fromResource(myMarkerStore[markerStoreID].getIcon())));
        myMarkerStore[markerStoreID].setId(markerStoreID);
        myMarkerStore[markerStoreID].setMarker(testMarker);
        Global.numMarkers++;
        if (D) Log.d(debugTag, "Num Markers = " + (Global.numMarkers - 1));

        return markerStoreID;
    }

    public String readEditText(EditText et) {
        return et.getText().toString();
    }

    /**
     * This function will enable the built in map controls and gestures.
     * Function will also turn on built in compass for rotation.
     *
     * @since 1.0.0
     * 
     * Modified 3/12/2015 Josh Klodnicki
     */
    private void turnOnMapControls() {
        GoogleMap googleMap = getTheDamnMap();

        //Enable compass and zoom controls
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.setMyLocationEnabled(true);

        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
            	// Update the screen coordinates every time the camera changes
            	updateScreenCoordinates();
            	
            	GoogleMap bigMap = getTheDamnMap();
                
                if (gridlines != null) gridlines.remove();
                gridlines = bigMap.addPolyline(Gridlines.generateGridlines());
                gridlines.setWidth(2);
            	
            	//TODO: adjust minimap based on where new position is
                if (miniMapBoundingBox != null) miniMapBoundingBox.remove();
                miniMapBoundingBox = null;
                FragmentManager fragmentManager = getFragmentManager();
                MapFragment mapFragment = (MapFragment) fragmentManager.findFragmentById(R.id.miniMap);
                GoogleMap miniMap = mapFragment.getMap();
                miniMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                float currZoom = bigMap.getCameraPosition().zoom;
                if (currZoom < 11) miniMap.moveCamera(CameraUpdateFactory.zoomTo(2));
                if (currZoom > 10 && currZoom < 14) miniMap.moveCamera(CameraUpdateFactory.zoomTo(6));
                if (currZoom > 14) miniMap.moveCamera(CameraUpdateFactory.zoomTo(11));
                
                miniMapBoundingBox = miniMap.addPolyline(new PolylineOptions()
                        .add(ScreenCoordinates.topLeft)
                        .add(ScreenCoordinates.topRight)
                        .add(ScreenCoordinates.bottomRight)
                        .add(ScreenCoordinates.bottomLeft)
                        .add(ScreenCoordinates.topLeft));
                miniMapBoundingBox.setWidth(2);
                if (isGridManagerRunning) {
                    myAsyncTask.cancel(true);
                }
                updateMarkerGroups();
            }
        });


        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @SuppressWarnings("ConstantConditions")
            @Override
            public boolean onMarkerClick(Marker marker) {

                marker.showInfoWindow();

                changeDeletableMarker(marker);
                addToContainer(marker);
                LatLng markerLL = marker.getPosition();
                LatLonPoint latLonPoint = new LatLonPoint(markerLL.latitude, markerLL.longitude);

                //This is where we actually handle the distance between points stuff
                if (distanceBetweenCoordsActive) {
                    if (distanceBetweenCoordsNum == 0) {
                        distanceBetweenCoordsPt1 = markerLL;

                        Global.dLMarker1ID = makeNewMarkerPleaseReturnInt(R.drawable.distance_marker_green_pin, "Location 1", "", markerLL);
                        //drop pin 1
                        distanceBetweenCoordsNum++;
                        TextView textView = (TextView) findViewById(R.id.distanceBtwnTV);
                        textView.setText("Choose second point");
                    } else if (distanceBetweenCoordsNum == 1) {
                        distanceBetweenCoordsPt2 = markerLL;
                        double distance = distanceBetweenCoords(distanceBetweenCoordsPt1, distanceBetweenCoordsPt2);

                        String distUnit = sharedPref.getString("prefUnitsFormat", "1");

                        //drop pin 2
                        Global.dLMarker2ID = makeNewMarkerPleaseReturnInt(R.drawable.distance_marker_green_pin, "Location 2", "", markerLL);
                        distanceBetweenCoordsNum = 0;
                        distanceBetweenCoordsPt1 = null;
                        distanceBetweenCoordsPt2 = null;
                        Button distanceBetweenCoordsBut = (Button) findViewById(R.id.distanceBtwnButton);
                        distanceBetweenCoordsBut.setText("Distance Between Points");

                        TextView textView = (TextView) findViewById(R.id.distanceBtwnTV);
                        if (distUnit.matches("1")) {
                            textView.setText("Distance = " + distance + " miles");
                        } else {
                            textView.setText("Distance = " + distance + " km");
                        }
                        PolylineOptions distanceLineOptions = new PolylineOptions().add(myMarkerStore[Global.dLMarker1ID].getLatLng()).add(myMarkerStore[Global.dLMarker2ID].getLatLng());
                        distLine = getTheDamnMap().addPolyline(distanceLineOptions);
                        distanceMarkersPresent = true;
                        //the user will only be allowed one of these at a time and if they do not delete the old one they will have to remove them individually

                    }
                }

                Coordinates.convertLL_to_MGRS(latLonPoint);
                String lat = String.valueOf(marker.getPosition().latitude);
                String lon = String.valueOf(marker.getPosition().longitude);
                setSidebarMarkerInfo(marker.getTitle(), marker.getSnippet(), lat, lon, Coordinates.convertLL_to_MGRS(latLonPoint));
                enableMarkersOptions();
                return false;
            }
        });

        //noinspection ConstantConditions
        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                //Toast.makeText(FirstActivity.this,"LC+ "+latLng.toString(),Toast.LENGTH_LONG).show();

                //TODO: make sure we chack whether to put down lat/lng or mgrs
                if (Global.currentSideBarLayout == SIDEBARLAYOUTTYPE.MAPMARKERS.intVal) {

                    EditText titleET = (EditText) findViewById(R.id.sideberMarkerTitleET);
                    EditText snippitET = (EditText) findViewById(R.id.sideberMarkerSnippitET);
                    String title = titleET.getText().toString();
                    String snippit = snippitET.getText().toString();
                    final RadioGroup coordTypeRadioGroup = (RadioGroup) findViewById(R.id.goToTypeRadioGroup);

                    switch (coordTypeRadioGroup.getCheckedRadioButtonId()) {
                        case R.id.mgrsCoordTypeRB:
                            EditText etMGRS = (EditText) findViewById(R.id.markersEnterMGRSET);
                            //TODO:MAKE SURE THE TEXT IS LEGAL
                            if (!etMGRS.getText().toString().isEmpty() && Coordinates.validMGRS(etMGRS.getText().toString())) {
                                Coordinates.convertLL_to_MGRS(new LatLonPoint(latLng.latitude, latLng.longitude));

                                if (title.contentEquals("")) {
                                    title = sharedPref.getString("defaultMarkerTitle", "Untitled Marker");
                                }
                                if (snippit.contentEquals("")) {
                                    snippit = sharedPref.getString("defaultMarkerSnippit", "") + " \n "
                                            + Coordinates.convertLL_to_MGRS(new LatLonPoint(latLng.latitude, latLng.longitude))
                                            + " \n "
                                            + Utilities.getTimeStamp();

                                }

                                makeNewMarker(currentChoice[0].drawable, title, snippit, latLng);
                            }
                            break;
                        case R.id.latLonCoordTypeRB:
                            EditText etLon = (EditText) findViewById(R.id.markersEnterLonET);
                            EditText etLat = (EditText) findViewById(R.id.markersEnterLatET);
                            if (!etLat.getText().toString().isEmpty() && !etLon.getText().toString().isEmpty()) {
                                //TODO:MAKE SURE THE TEXT IS LEGAL
                                //TODO:PUT TRY CATCH BLOCK AROUND CONVERSION

                                if (title.contentEquals("")) {
                                    title = sharedPref.getString("defaultMarkerTitle", "Untitled Marker");
                                }
                                if (snippit.contentEquals("")) {
                                    snippit = sharedPref.getString("defaultMarkerSnippit", "") + " \n " + latLng.toString() + " \n " + Utilities.getTimeStamp();
                                }

                                makeNewMarker(currentChoice[0].drawable, title, snippit, latLng);
                            }
                            break;
                    }

                }
            }
        });

        //noinspection ConstantConditions
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                dropNewContainer(latLng);
                LatLonPoint latLonPoint = new LatLonPoint(latLng.latitude, latLng.longitude);
                Coordinates.convertLL_to_MGRS(latLonPoint);
                String lat = String.valueOf(latLonPoint.getLatitude());
                String lon = String.valueOf(latLonPoint.getLongitude());
                setSidebarMarkerInfo("", "", lat, lon, Coordinates.convertLL_to_MGRS(latLonPoint));

                //setSidebarMarkerInfo("","","","","");//delete all info from sidebar about old markers

                //This is where we actually handle the distance between points stuff
                if (distanceBetweenCoordsActive) {
                    if (distanceBetweenCoordsNum == 0) {
                        distanceBetweenCoordsPt1 = latLng;

                        Global.dLMarker1ID = makeNewMarkerPleaseReturnInt(R.drawable.distance_marker_green_pin, "Location 1", "", latLng);
                        //drop pin 1
                        distanceBetweenCoordsNum++;
                        TextView textView = (TextView) findViewById(R.id.distanceBtwnTV);
                        textView.setText("Choose second point");
                    } else if (distanceBetweenCoordsNum == 1) {
                        distanceBetweenCoordsPt2 = latLng;
                        double distance = distanceBetweenCoords(distanceBetweenCoordsPt1, distanceBetweenCoordsPt2);

                        String distUnit = sharedPref.getString("prefUnitsFormat", "1");

                        //drop pin 2
                        Global.dLMarker2ID = makeNewMarkerPleaseReturnInt(R.drawable.distance_marker_green_pin, "Location 2", "", latLng);
                        distanceBetweenCoordsNum = 0;
                        distanceBetweenCoordsPt1 = null;
                        distanceBetweenCoordsPt2 = null;
                        Button distanceBetweenCoordsBut = (Button) findViewById(R.id.distanceBtwnButton);
                        distanceBetweenCoordsBut.setText("Distance Between Points");

                        TextView textView = (TextView) findViewById(R.id.distanceBtwnTV);
                        if (distUnit.matches("1")) {
                            textView.setText("Distance = " + distance + " miles");
                        } else {
                            textView.setText("Distance = " + distance + " km");
                        }
                        PolylineOptions distanceLineOptions = new PolylineOptions().add(myMarkerStore[Global.dLMarker1ID].getLatLng()).add(myMarkerStore[Global.dLMarker2ID].getLatLng());
                        distLine = getTheDamnMap().addPolyline(distanceLineOptions);
                        distanceMarkersPresent = true;
                        //the user will only be allowed one of these at a time and if they do not delete the old one they will have to remove them individually

                    }
                }
            }
        });

        FragmentManager fm = getFragmentManager();
        MapFragment mapFrag = (MapFragment) fm.findFragmentById(R.id.miniMap);
        GoogleMap miniMap = mapFrag.getMap();
        miniMap.getUiSettings().setCompassEnabled(true);
        miniMap.getUiSettings().setMyLocationButtonEnabled(false);
        miniMap.getUiSettings().setRotateGesturesEnabled(true);
        miniMap.getUiSettings().setZoomControlsEnabled(false);
        miniMap.getUiSettings().setZoomGesturesEnabled(true);
        miniMap.getUiSettings().setScrollGesturesEnabled(true);
        miniMap.getUiSettings().setTiltGesturesEnabled(false);
        miniMap.setMyLocationEnabled(false);

    }

    
    
    /**
     * This method will clear any present distance markers and reset the sidebar to a state
     * where no distance markers are present and the function is turned off.
     *
     * @since 1.0.0
     */
    private void clearDistanceMarkers() {
        Button distanceBetweenCoordsBut = (Button) findViewById(R.id.distanceBtwnButton);
        TextView textView = (TextView) findViewById(R.id.distanceBtwnTV);

        //this will handle if the user wants to cancel distance without placing any markers
        if (distanceBetweenCoordsNum == 0 && !distanceMarkersPresent && distanceBetweenCoordsActive) {
            distanceBetweenCoordsBut.setText("Distance Between Points");
            distanceBetweenCoordsNum = 0;
            textView.setText("");
            distanceBetweenCoordsActive = false;
            distanceMarkersPresent = false;
        }

        //this will handle is the user wants to cancel distance when 1 marker has been placed
        if (distanceBetweenCoordsNum == 1 && !distanceMarkersPresent && distanceBetweenCoordsActive) {
            distanceBetweenCoordsBut.setText("Distance Between Points");
            distanceBetweenCoordsNum = 0;
            textView.setText("");
            distanceBetweenCoordsActive = false;
            distanceMarkersPresent = false;
            myMarkerStore[Global.dLMarker1ID].getMarker().remove();
        }

        //this handles clearing the distance markers if both have been placed.
        if (distanceMarkersPresent) {
            myMarkerStore[Global.dLMarker1ID].getMarker().remove();
            myMarkerStore[Global.dLMarker2ID].getMarker().remove();
            distLine.remove();
            distanceMarkersPresent = false;
        }
        textView.setText("");
    }

    /**
     * This function sets the information in the marker page of the sidebar to the provided inputs.
     *
     * @param title   String title
     * @param snippit String snippit
     * @param lat     String lat
     * @param lon     String lon
     * @param mgrs    String mgrs
     * @since 1.0.0
     */
    private void setSidebarMarkerInfo(String title, String snippit, String lat, String lon, String mgrs) {
        if (!title.matches("My Position")) {
            EditText sbtitleET = (EditText) findViewById(R.id.sideberMarkerTitleET);
            EditText sbsnippitET = (EditText) findViewById(R.id.sideberMarkerSnippitET);
            sbtitleET.setText(title);
            sbsnippitET.setText(snippit);
        }
        EditText latET = (EditText) findViewById(R.id.markersEnterLatET);
        EditText lonET = (EditText) findViewById(R.id.markersEnterLonET);
        EditText mgrsET = (EditText) findViewById(R.id.markersEnterMGRSET);

        //truncating out extraneous digits
        if (lat.startsWith("-") && lat.length() > 10) {
            lat = lat.substring(0, 9);
        } else {
            if (lat.length() > 9) {
                lat = lat.substring(0, 8);
            }
        }

        if (lon.startsWith("-") && lon.length() > 10) {
            lon = lon.substring(0, 9);
        } else {
            if (lon.length() > 9) {
                lon = lon.substring(0, 8);
            }
        }
        //end truncating

        latET.setText(lat);
        lonET.setText(lon);
        mgrsET.setText(mgrs);
    }

    /**
     * Create the menu or action bar from the menu.xml file.
     *
     * @param menu the options menu to be created
     * @return nothing really
     * @since 1.0.0
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (D) Log.d(debugTag, "onCreateOptionsMenu()");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    /**
     * Controls the listeners for the menu/action bar.
     * This is used to reset the sidebar functionality also.
     *
     * @param item The menuitem selected
     * @return nothing really
     * @since 1.0.0
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.clearMap:
                clearDistanceMarkers();
                clearMarkerStore();
                getTheDamnMap().clear();
                break;
            case R.id.smp:
                //show current user position on map
                showMyLocaton();
                break;
            case R.id.toggleSidebar:
                //Toggle sidebar on/off
                toggleSideBar();
                break;
            case R.id.toggleMapTypes:
                if (!sidebarOpen) toggleSideBar();
                enableMapTypeOptions();
                break;
            //this button only used for debug and will be removed in final version
            //TODO:REMOVE!
            case R.id.debugMenuBut:
                //if (!sidebarOpen) toggleSideBar();
                //doAToast("zoom = " + getTheDamnMap().getCameraPosition().zoom, 1);
                //saveAllMarkers();
              /*  if (whereIveBeenOn) {
                    whereIveBeenOn = false;
                    if (whereIveBeenPL != null) whereIveBeenPL.remove();
                    doAToast("distance = " + whereIveBeenDistance, 1);
                    whereIveBeenDistance = 0;
                    whereIveBeenLastPoint = null;
                    whereIveBeenNewPoint = null;
                } else {
                    whereIveBeenOn = true;
                    whereIveBeenPL = getTheDamnMap().addPolyline(new PolylineOptions()
                            .add(new LatLng(Global.CURRENT_LAT, Global.CURRENT_LON), new LatLng(Global.CURRENT_LAT * 1, Global.CURRENT_LON * 1))
                            .width(5)
                            .color(Color.RED)
                    );*/
/*                    if(whereIveBeenLastPoint==null&&whereIveBeenNewPoint!=null){
                        whereIveBeenLastPoint=whereIveBeenNewPoint;
                    }
                    else if(whereIveBeenNewPoint==null&&whereIveBeenLastPoint==null){
                        whereIveBeenDistance=0;
                        whereIveBeenNewPoint=new LatLng(Global.CURRENT_LAT, Global.CURRENT_LON);
                    }
                    else{
                        LatLng newLL = new LatLng(Global.CURRENT_LAT, Global.CURRENT_LON);
                        whereIveBeenDistance+=distanceBetweenCoords(whereIveBeenLastPoint,whereIveBeenNewPoint);
                        whereIveBeenLastPoint=whereIveBeenNewPoint;
                        whereIveBeenNewPoint=newLL;
                    }*/

                //}

                //TODO:WRITE WIFI TO FILE
                writeWifiList();

                break;
            case R.id.saveMarkersBut:
                saveAllMarkers();
                break;
            case R.id.restoreMarkersBut:
                reinstateMarkersFromFile("savedmarkers.jml");
                break;
            case R.id.toggleMarkers:
                if (!sidebarOpen) toggleSideBar();
                enableMarkersOptions();
                break;
            case R.id.toggleDistance:
                if (!sidebarOpen) toggleSideBar();
                enableDistanceOptions();
                break;
            case R.id.settingsMenuBut:
                //Switch to settings activity
                Intent myIntent = new Intent(FirstActivity.this,
                        SettingsActivity.class);
                startActivityForResult(myIntent, 0);
                break;
            case R.id.lightToggleBut:
                if (!lightOn) {
                    flashlight = new Flashlight(this);
                    addContentView(flashlight.getSurfaceView(), new ViewGroup.LayoutParams(1, 1));
                    flashlight.setTorch(true);
                    lightOn = true;
                } else {
                    flashlight.setTorch(false);
                    flashlight.stop();
                    lightOn = false;
                }
                break;
            case R.id.saveRouteBut:
                if (whereIveBeenPL != null) {
                    writeToDownloadDirectory("MyRoute " + Utilities.getTimeStamp() + ".txt", whereIveBeenPL.getPoints().toString());
                }
                break;
        }
        return true;
    }

    /**
     * This method will reset all entries in the marker store to null, in essence "clearing" it
     *
     * @since 1.0.0
     */
    private void clearMarkerStore() {
        for (int i = myMarkerStore.length - 1; i > 0; i--) {

            if (myMarkerStore[i] != null) {
                myMarkerStore[i] = null;
            }
        }
    }

    /**
     * This function updates the current screen coordinates
     *
     * @since 1.0.0
     * 
     * Modified 3/12/2015 Josh Klodnicki
     *          4/21/2015
     */
    public void updateScreenCoordinates() {
        int mapWidth  = findViewById(R.id.map).getWidth();
        int mapHeight = findViewById(R.id.map).getHeight();
        
        ScreenCoordinates.topLeft     = Global.gmap.getProjection().fromScreenLocation(new Point(0, 0));
        ScreenCoordinates.topRight    = Global.gmap.getProjection().fromScreenLocation(new Point(mapWidth, 0));
        ScreenCoordinates.bottomLeft  = Global.gmap.getProjection().fromScreenLocation(new Point(0, mapHeight));
        ScreenCoordinates.bottomRight = Global.gmap.getProjection().fromScreenLocation(new Point(mapWidth, mapHeight));
        ScreenCoordinates.center      = Global.gmap.getProjection().fromScreenLocation(new Point(mapWidth/2, mapHeight/2));
    }

    /**
     * This function will set the remove button in the sidebar to remove the last selected marker as passed into this function.
     * No markers can be removed until one has been clicked on and "activated"
     *
     * @param marker The marker that we want to be able to delete
     * @since 1.0.0
     */
    public void changeDeletableMarker(final Marker marker) {
        Button removeBut = (Button) findViewById(R.id.removeMarkerButton);
        removeBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String markID = marker.getId();
                int markerID;
                try {
                    if (marker.getTitle().contains("My Position")) {
                        deleteMyPositionMarker();
                    } else {
                        markerID = Integer.parseInt(markID.substring(1));
                        FirstActivity.myMarkerStore[markerID] = null;
                        marker.remove();
                    }
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
            }
        });
    }

    /**
     * This function checks to make sure the Google Play Services are successfully added to the project.
     * The output will be a bool and also to the Android Logcat.
     *
     * @return True if the play services are added to the project correctly, False otherwise.
     * @since 1.0.0
     */
    public boolean checkForPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        boolean isAdded = false;
        if (status == ConnectionResult.SUCCESS) {
            if (D) Log.d(debugTag, "Play Services Found");
            isAdded = true;
        } else {
            Log.e(debugTag, "Play Services Not Found");
        }
        return isAdded;
    }

    /**
     * This function will display the dialog box to allow the user to choose a new main symbol to drop.
     * Upon clicking any symbol in the grid EXCEPT THE TOP LEFT, the top left symbol will switch to that
     * selected image. When you click the top left image this will be relayed to the main app to be used
     * as the primary symbol for dropping markers. The top left will also close the dialog.
     * Be aware if you tap outside the dialog no selection will be made.
     *
     * @since 1.0.0
     */
    public void enableSymbolChooserDialog() {

        //make a new symbol chooser dialog box using our layout
        final Dialog d = new Dialog(FirstActivity.this, R.style.SymbolChooserDialogTheme);
        d.setContentView(R.layout.symbolchooser);

        //main imagebutton from the sidebar
        final ImageButton ibMain = (ImageButton) findViewById(R.id.sidebarSymbolDisplayIB);

        //top left image buttion in dialog
        final ImageButton ibChosen = (ImageButton) d.findViewById(R.id.ib_chosensymbol);

        //16 imagebuttons representing the 16 standard symbols
        ImageButton ibFriendlyGround = (ImageButton) d.findViewById(R.id.ib_friend_ground);
        ImageButton ibFriendlySeaSurface = (ImageButton) d.findViewById(R.id.ib_friend_seasurface);
        ImageButton ibFriendlyAirSpace = (ImageButton) d.findViewById(R.id.ib_friend_airspace);
        ImageButton ibFriendlySubsurface = (ImageButton) d.findViewById(R.id.ib_friend_subsurface);
        ImageButton ibHostileGround = (ImageButton) d.findViewById(R.id.ib_hostile_ground);
        ImageButton ibHostileSeaSurface = (ImageButton) d.findViewById(R.id.ib_hostile_seasurface);
        ImageButton ibHostileAirSpace = (ImageButton) d.findViewById(R.id.ib_hostile_airspace);
        ImageButton ibHostileSubsurface = (ImageButton) d.findViewById(R.id.ib_hostile_subsurface);
        ImageButton ibNeutralGround = (ImageButton) d.findViewById(R.id.ib_neutral_ground);
        ImageButton ibNeutralSeaSurface = (ImageButton) d.findViewById(R.id.ib_neutral_seasurface);
        ImageButton ibNeutralAirSpace = (ImageButton) d.findViewById(R.id.ib_neutral_airspace);
        ImageButton ibNeutralSubsurface = (ImageButton) d.findViewById(R.id.ib_neutral_subsurface);
        ImageButton ibUnknownGround = (ImageButton) d.findViewById(R.id.ib_unknown_ground);
        ImageButton ibUnknownSeaSurface = (ImageButton) d.findViewById(R.id.ib_unknown_seasurface);
        ImageButton ibUnknownAirSpace = (ImageButton) d.findViewById(R.id.ib_unknown_airspace);
        ImageButton ibUnknownSubsurface = (ImageButton) d.findViewById(R.id.ib_unknown_subsurface);

        ibChosen.setImageResource(currentChoice[0].drawable);

        //set up all the listeners for the buttons
        //when one of these is selected the top left image will change to the tapped image
        //currentChoice[0] will be updated with the new selection.
        ibFriendlyGround.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ibChosen.setImageResource(R.drawable.friend_ground);
                currentChoice[0] = SYMBOLDICT.FriendlyGround;
                return false;
            }
        });

        ibFriendlySeaSurface.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ibChosen.setImageResource(R.drawable.friend_seasurface);
                currentChoice[0] = SYMBOLDICT.FriendlySeaSurface;
                return false;
            }
        });

        ibFriendlyAirSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.friend_airspace);
                currentChoice[0] = FriendlyAirSpace;
            }
        });

        ibFriendlySubsurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.friend_subsurface);
                currentChoice[0] = SYMBOLDICT.FriendlySubsurface;
            }
        });

        ibHostileGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.hostile_ground);
                currentChoice[0] = SYMBOLDICT.HostileGround;
            }
        });

        ibHostileSeaSurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.hostile_seasurface);
                currentChoice[0] = SYMBOLDICT.HostileSeaSurface;
            }
        });

        ibHostileAirSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.hostile_airspace);
                currentChoice[0] = SYMBOLDICT.HostileAirSpace;
            }
        });

        ibHostileSubsurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.hostile_subsurface);
                currentChoice[0] = SYMBOLDICT.HostileSubsurface;
            }
        });

        ibNeutralGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.neutral_ground);
                currentChoice[0] = SYMBOLDICT.NeutralGround;
            }
        });

        ibNeutralSeaSurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.neutral_seasurface);
                currentChoice[0] = SYMBOLDICT.NeutralSeaSurface;
            }
        });

        ibNeutralAirSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.neutral_airspace);
                currentChoice[0] = SYMBOLDICT.NeutralAirSpace;
            }
        });

        ibNeutralSubsurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.neutral_subsurface);
                currentChoice[0] = SYMBOLDICT.NeutralSubsurface;
            }
        });

        ibUnknownGround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.unknown_ground);
                currentChoice[0] = SYMBOLDICT.UnknownGround;
            }
        });

        ibUnknownSeaSurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.unknown_seasurface);
                currentChoice[0] = SYMBOLDICT.UnknownSeaSurface;
            }
        });

        ibUnknownAirSpace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.unknown_airspace);
                currentChoice[0] = SYMBOLDICT.UnknownAirSpace;
            }
        });

        ibUnknownSubsurface.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosen.setImageResource(R.drawable.unknown_subsurface);
                currentChoice[0] = SYMBOLDICT.UnknownSubsurface;
            }
        });

        //This is the listener on the top left symbol.
        //upon it being clicked the image in the sidebar will be updated to the current
        //selection and that should become the new image for markers.
        ibChosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (D) Log.d(debugTag, "chosen image: " + currentChoice[0].toString());

                switch (currentChoice[0]) {
                    case FriendlyGround:
                        ibMain.setImageResource(R.drawable.friend_ground);
                        break;
                    case FriendlySeaSurface:
                        ibMain.setImageResource(R.drawable.friend_seasurface);
                        break;
                    case FriendlyAirSpace:
                        ibMain.setImageResource(R.drawable.friend_airspace);
                        break;
                    case FriendlySubsurface:
                        ibMain.setImageResource(R.drawable.friend_subsurface);
                        break;
                    case HostileGround:
                        ibMain.setImageResource(R.drawable.hostile_ground);
                        break;
                    case HostileSeaSurface:
                        ibMain.setImageResource(R.drawable.hostile_seasurface);
                        break;
                    case HostileAirSpace:
                        ibMain.setImageResource(R.drawable.hostile_airspace);
                        break;
                    case HostileSubsurface:
                        ibMain.setImageResource(R.drawable.hostile_subsurface);
                        break;
                    //if you are reading this you have found the bellows of hell.
                    case NeutralGround:
                        ibMain.setImageResource(R.drawable.neutral_ground);
                        break;
                    case NeutralSeaSurface:
                        ibMain.setImageResource(R.drawable.neutral_seasurface);
                        break;
                    case NeutralAirSpace:
                        ibMain.setImageResource(R.drawable.neutral_airspace);
                        break;
                    case NeutralSubsurface:
                        ibMain.setImageResource(R.drawable.neutral_subsurface);
                        break;
                    case UnknownGround:
                        ibMain.setImageResource(R.drawable.unknown_ground);
                        break;
                    case UnknownSeaSurface:
                        ibMain.setImageResource(R.drawable.unknown_seasurface);
                        break;
                    case UnknownAirSpace:
                        ibMain.setImageResource(R.drawable.unknown_airspace);
                        break;
                    case UnknownSubsurface:
                        ibMain.setImageResource(R.drawable.unknown_subsurface);
                        break;
                }
                //close the dialog box
                d.dismiss();
            }
        });
        //we have set everything up show the box.
        d.show();
    }

    /**
     * This function will toggle the side bar visible or gone.
     * When the sidebar is made 'gone' the map will fill screen.
     *
     * @since 1.0.0
     */
    private void toggleSideBar() {
        LinearLayout sidebarLL = (LinearLayout) findViewById(R.id.mapSideBarLL);
        if (sidebarLL.getVisibility() == View.VISIBLE) {
            sidebarLL.setVisibility(View.GONE);
            sidebarOpen = false;
        } else if (sidebarLL.getVisibility() == View.GONE) {
            sidebarLL.setVisibility(View.VISIBLE);
            sidebarOpen = true;
        }
    }

    private void enableDistanceOptions() {
        TextView sidebarHeader = (TextView) findViewById(R.id.sideBarHeaderTV);
        sidebarHeader.setText(R.string.mapDistanceHeader);
        LinearLayout distanceOptions = (LinearLayout) findViewById(R.id.mapDistanceLL);
        if (Global.currentSideBarLayout != SIDEBARLAYOUTTYPE.MAPDISTANCE.intVal) {
            //turn off other linear layouts before you enable a new one
            LinearLayout mapMarkersLayout = (LinearLayout) findViewById(R.id.markersLL);
            LinearLayout mapTypeLayout = (LinearLayout) findViewById(R.id.mapTypeLL);
            mapMarkersLayout.setVisibility(View.GONE);
            mapTypeLayout.setVisibility(View.GONE);

            distanceOptions.setVisibility(View.VISIBLE);
            Global.currentSideBarLayout = SIDEBARLAYOUTTYPE.MAPDISTANCE.intVal;
            enableMapTypeListeners();
        }

        enableDistanceOptionsListeners();
    }

    /**
     * This function will enable the map types functionality and change appropriate layouts
     *
     * @since 1.0.0
     */
    private void enableMapTypeOptions() {
        TextView sidebarHeader = (TextView) findViewById(R.id.sideBarHeaderTV);
        sidebarHeader.setText(R.string.mapTypeHeader);
        LinearLayout mapTypeLayout = (LinearLayout) findViewById(R.id.mapTypeLL);
        if (Global.currentSideBarLayout != SIDEBARLAYOUTTYPE.MAPTYPES.intVal) {
            //turn off other linear layouts before you enable a new one
            LinearLayout mapMarkersLayout = (LinearLayout) findViewById(R.id.markersLL);
            LinearLayout distanceOptions = (LinearLayout) findViewById(R.id.mapDistanceLL);

            mapMarkersLayout.setVisibility(View.GONE);
            distanceOptions.setVisibility(View.GONE);

            mapTypeLayout.setVisibility(View.VISIBLE);
            Global.currentSideBarLayout = SIDEBARLAYOUTTYPE.MAPTYPES.intVal;
            enableMapTypeListeners();
        }

    }

    /**
     * This function will enable map marker options and change appropriate layouts
     *
     * @since 1.0.0
     */
    private void enableMarkersOptions() {
        TextView sidebarHeader = (TextView) findViewById(R.id.sideBarHeaderTV);
        sidebarHeader.setText(R.string.mapMarkersTitle);
        LinearLayout mapMarkersLayout = (LinearLayout) findViewById(R.id.markersLL);
        if (Global.currentSideBarLayout != SIDEBARLAYOUTTYPE.MAPMARKERS.intVal) {
            //turn off other linear layouts before you enable a new one
            LinearLayout mapTypeLayout = (LinearLayout) findViewById(R.id.mapTypeLL);
            LinearLayout distanceOptions = (LinearLayout) findViewById(R.id.mapDistanceLL);

            distanceOptions.setVisibility(View.GONE);
            mapTypeLayout.setVisibility(View.GONE);

            mapMarkersLayout.setVisibility(View.VISIBLE);
            Global.currentSideBarLayout = SIDEBARLAYOUTTYPE.MAPMARKERS.intVal;
            enableMapMarkersListeners();
        }
    }

    /**
     * This function enabled the listeners on the map markers sidebar page
     *
     * @since 1.0.0
     */
    private void enableMapMarkersListeners() {
        final RadioGroup coordTypeRadioGroup = (RadioGroup) findViewById(R.id.goToTypeRadioGroup);
        final LinearLayout mgrsLL = (LinearLayout) findViewById(R.id.markerMGRSCoordsLL);
        final LinearLayout markersLonCoordsLL = (LinearLayout) findViewById(R.id.markersLonCoordsLL);
        final LinearLayout markersLatCoordsLL = (LinearLayout) findViewById(R.id.markersLatCoordsLL);
        final LinearLayout spacer = (LinearLayout) findViewById(R.id.markerCoordSpacer);

        coordTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.mgrsCoordTypeRB:
                        markersLatCoordsLL.setVisibility(View.GONE);
                        markersLonCoordsLL.setVisibility(View.GONE);
                        mgrsLL.setVisibility(View.VISIBLE);
                        spacer.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.latLonCoordTypeRB:
                        markersLatCoordsLL.setVisibility(View.VISIBLE);
                        markersLonCoordsLL.setVisibility(View.VISIBLE);
                        mgrsLL.setVisibility(View.GONE);
                        spacer.setVisibility(View.GONE);
                        break;
                }

            }
        });

        //set the go to coords button to see which method is checked and call appropriate function
        Button goToCoordButton = (Button) findViewById(R.id.goToCoordsButton);
        goToCoordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (coordTypeRadioGroup.getCheckedRadioButtonId()) {
                    case R.id.mgrsCoordTypeRB:
                        EditText etMGRS = (EditText) findViewById(R.id.markersEnterMGRSET);
                        if (!etMGRS.getText().toString().isEmpty() && Coordinates.validMGRS(etMGRS.getText().toString())) {
                            LatLonPoint llpt = Coordinates.convertMGRS_to_LatLonPoint(etMGRS.getText().toString());
                            animateToLoc(llpt.getLatitude(), llpt.getLongitude());
                        }
                        break;
                    case R.id.latLonCoordTypeRB:
                        EditText etLon = (EditText) findViewById(R.id.markersEnterLonET);
                        EditText etLat = (EditText) findViewById(R.id.markersEnterLatET);
                        //TODO:MAKE SURE THE TEXT IS LEGAL
                        if (!etLat.getText().toString().isEmpty() && !etLon.getText().toString().isEmpty()) {
                            animateToLoc(parseDouble(etLat.getText().toString()), parseDouble(etLon.getText().toString()));
                        }
                        break;
                }
            }
        });

        Button dropAtCoordsButton = (Button) findViewById(R.id.dropAtCoordsButton);
        dropAtCoordsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText titleET = (EditText) findViewById(R.id.sideberMarkerTitleET);
                EditText snippitET = (EditText) findViewById(R.id.sideberMarkerSnippitET);
                String title = titleET.getText().toString();
                String snippit = snippitET.getText().toString();

                switch (coordTypeRadioGroup.getCheckedRadioButtonId()) {
                    case R.id.mgrsCoordTypeRB:
                        EditText etMGRS = (EditText) findViewById(R.id.markersEnterMGRSET);
                        //TODO:MAKE SURE THE TEXT IS LEGAL
                        if (!etMGRS.getText().toString().isEmpty() && Coordinates.validMGRS(etMGRS.getText().toString())) {

                            LatLonPoint llpt = Coordinates.convertMGRS_to_LatLonPoint(etMGRS.getText().toString());
                            animateToLoc(llpt.getLatitude(), llpt.getLongitude());

                            LatLng latLngM = new LatLng(llpt.getLatitude(), llpt.getLongitude());

                            if (title.contentEquals("")) {
                                title = "Untitled Marker";
                            }
                            if (snippit.contentEquals("")) {
                                snippit = etMGRS.getText().toString() + " \n " + Utilities.getTimeStamp();
                            }

                            makeNewMarker(currentChoice[0].drawable, title, snippit, latLngM);
                        }
                        break;
                    case R.id.latLonCoordTypeRB:
                        EditText etLon = (EditText) findViewById(R.id.markersEnterLonET);
                        EditText etLat = (EditText) findViewById(R.id.markersEnterLatET);
                        if (!etLat.getText().toString().isEmpty() && !etLon.getText().toString().isEmpty()) {
                            //TODO:MAKE SURE THE TEXT IS LEGAL
                            //TODO:PUT TRY CATCH BLOCK AROUND CONVERSION
                            animateToLoc(parseDouble(etLat.getText().toString()), parseDouble(etLon.getText().toString()));

                            LatLng latLng = new LatLng(parseDouble(etLat.getText().toString()), parseDouble(etLon.getText().toString()));

                            if (title.contentEquals("")) {
                                title = "Untitled Marker";
                            }
                            if (snippit.contentEquals("")) {
                                snippit = latLng.toString() + " \n " + Utilities.getTimeStamp();
                            }

                            makeNewMarker(currentChoice[0].drawable, title, snippit, latLng);
                        }
                        break;
                }
            }
        });

        ImageButton symbolSelectorIB = (ImageButton) findViewById(R.id.sidebarSymbolDisplayIB);
        symbolSelectorIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableSymbolChooserDialog();
            }
        });

        final Button distanceBetweenCoordsBut = (Button) findViewById(R.id.distanceBtwnButton);
        distanceBetweenCoordsBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearDistanceMarkers();
                distanceBetweenCoordsActive = true;
                distanceBetweenCoordsBut.setText("Distance Between Points Active");
                TextView textView = (TextView) findViewById(R.id.distanceBtwnTV);
                textView.setText("Choose first point");
                textView.setVisibility(View.VISIBLE);
                //TODO:MAKE A WAY FOR THE USER TO CANCEL PLACING DISTANCE MARKERS
            }
        });

        final Button clearDistanceBetweenCoordsBut = (Button) findViewById(R.id.clearDistanceBtwnButton);
        clearDistanceBetweenCoordsBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = (TextView) findViewById(R.id.distanceBtwnTV);
                textView.setText("");
                textView.setVisibility(View.INVISIBLE);
                clearDistanceMarkers();
            }
        });

        final Button expandContainerBut = (Button) findViewById(R.id.expandContainerButton);
        expandContainerBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.testMMC.expandIntoOriginal();
            }
        });

    }

    private void enableDistanceOptionsListeners() {
        final Button toggleRouteTrackingBut = (Button) findViewById(R.id.toggleRouteTrackingBut);
        toggleRouteTrackingBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whereIveBeenOn) {
                    whereIveBeenOn = false;
                    if (whereIveBeenPL != null) whereIveBeenPL.remove();
                    doAToast("distance = " + whereIveBeenDistance, 1);
                    whereIveBeenDistance = 0;
                    whereIveBeenLastPoint = null;
                    whereIveBeenNewPoint = null;
                    toggleRouteTrackingBut.setText("Toggle Route Tracking");
                    if (!checkForDownloadsFiles("trackedRoutes.txt")) {
                        writeToDownloadDirectory("trackedRoutes.txt", "begin");
                    }
                    String oldtxt = readFromDownloadDirectory("trackedRoutes.txt");
                    writeToDownloadDirectory("trackedRoutes.txt", oldtxt + "\n" + Utilities.getTimeStamp() + " " + whereIveBeenDistance);

                } else {
                    whereIveBeenOn = true;
                    whereIveBeenPL = getTheDamnMap().addPolyline(new PolylineOptions()
                                    .add(new LatLng(Global.CURRENT_LAT, Global.CURRENT_LON), new LatLng(Global.CURRENT_LAT * 1, Global.CURRENT_LON * 1))
                                    .width(5)
                                    .color(Color.RED)
                    );
                    toggleRouteTrackingBut.setText("Route Tracking Active");
                }
            }
        });

        final Button goodBut = (Button) findViewById(R.id.toggleRouteGoodBut);
        goodBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("DistanceTesting", "Good");
            }
        });
        final Button badBut = (Button) findViewById(R.id.toggleRouteBadBut);
        badBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("DistanceTesting", "Bad");
            }
        });
    }

    /**
     * This function will enable the listeners in the map type linear layout
     *
     * @since 1.0.0
     */
    private void enableMapTypeListeners() {
        RadioGroup mapTypeRadioGroup = (RadioGroup) findViewById(R.id.mapTypeRadioGroup);

        mapTypeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                MapTypes.disableMap();
                switch (i) {
                    case R.id.mapTypeRBhybrid:
                        //enable hybrid map
                        MapTypes.enableHybridMap();
                        break;
                    case R.id.mapTypeRBnormal:
                        //enable normal map
                        MapTypes.enableNormalMap();
                        break;
                    case R.id.mapTypeRBsatellite:
                        //enable satellite map
                        MapTypes.enableSatMap();
                        break;
                    case R.id.mapTypeRBterrain:
                        //enable terrain map
                        MapTypes.enableTerrainMap();
                        break;
                    case R.id.mapTypeRBcustom:
                        //enable custom map type
                        //for now this will be the hybrid map
                        MapTypes.enableCustomMap();
                        break;
                    case R.id.mapTypeRBnone:
                        //disable map
                        //MapTypes.disableMap();
                        break;
                }
            }
        });

    }

    /**
     * This function will enable the built in my location controls and drop a pin on the current location
     * of the user, with a time stamp from the system clock.
     *
     * @since 1.0.0
     */
    private void showMyLocaton() {
        GoogleMap googleMap = getTheDamnMap();
        googleMap.setMyLocationEnabled(true);

        if (Global.CURRENT_LAT != 0 && Global.CURRENT_LON != 0) {
            if (Global.showMyPOSMarkerID != 999) deleteMyPositionMarker();
            LatLng userLatLong = new LatLng(Global.CURRENT_LAT, Global.CURRENT_LON);
            //makeNewMarker(currentChoice[0].drawable, "My Position", "My Location at ", userLatLong,0);
            String wifi = updateWifiList();
            Global.showMyPOSMarkerID = makeNewMarkerPleaseReturnInt(myMarkerStore[0].getIcon(), "My Position", "Wifi at my location:" + wifi, userLatLong);

            boolean zoomonShowPOS = sharedPref.getBoolean("zoomOnShowPosition", true);
            if (zoomonShowPOS) {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLong, 18));
            } else {
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLong, googleMap.getCameraPosition().zoom));
            }

        } else {
            Toast.makeText(FirstActivity.this, "GPS Signal Problem", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Perform a toast under the context of the main app. 1 for long, 0 for short.
     *
     * @param toBeToasted the string to be toasted.
     * @param howLong     int value for how long the toast should last. 1 for long 0 for short
     * @since 1.0.0
     */
    private void doAToast(String toBeToasted, int howLong) {
        if (D) Log.d(debugTag, "doAToast()");
        /*this will perform a toast given an input string "toBeToasted" and a duration.
        * Duration can be 1 for Long or 0 for Short.*/
        Toast.makeText(FirstActivity.this, toBeToasted, howLong).show();
    }

    public double roundTo3DecimalPlaces(double d) {
        DecimalFormat decimalFormat = new DecimalFormat("#.###");
        return Double.valueOf(decimalFormat.format(d));
    }

    /**
     * Update the Global.java file with the current coordinates as found by the device GPS.
     *
     * @param location is what is returned from the GPS locator. This value can be parsed into its components.
     * @since 1.0.0
     */
    private void updateLLfromGPS(Location location) {
        if (D) Log.d(debugTag, "updateLLfromGPS()");
        //Update all variables for the current GPS Location
        userLatitude = location.getLatitude();
        userLongitude = location.getLongitude();
        cameraAlt = location.getAltitude() + 1500;//add 1500 meters to view your current alt  well.

        Global.CURRENT_LAT = location.getLatitude();
        Global.CURRENT_LON = location.getLongitude();
        Global.CURRENT_ALT = location.getAltitude() + 1500;

        //Make a LatLonPoint out of the current user coordinates from the gps.
        android_OpenMap_Framework.common.LatLonPoint current_latLonPoint = new android_OpenMap_Framework.common.LatLonPoint();
        current_latLonPoint.setLatLon(userLatitude, userLongitude);
    }

    /**
     * Update the Global.java file with the current coordinates as found by the device GPS, but
     * from the Global.java Lat/Lon. This function should not be called by anything aside from the GPS thread.
     *
     * @return string value of the current MGRS coordinates of the device.
     * @since 1.0.0
     */
    private String updateMGRS() {
        if (D) Log.d(debugTag, "updateMGRS()");
        //Make a LatLonPoint out of the current user coordinates from the gps.
        android_OpenMap_Framework.common.LatLonPoint current_latLonPoint = new android_OpenMap_Framework.common.LatLonPoint();
        current_latLonPoint.setLatLon(userLatitude, userLongitude);
        String currentMGRS = Coordinates.convertLL_to_MGRS(current_latLonPoint);

        //Set the global as well
        Global.CURRENT_MGRS = currentMGRS;

        //used for testing
        if (T) doAToast("Current User Position = " + Global.CURRENT_MGRS, 0);

        return currentMGRS;
    }

    /**
     * This function will animate the map to an input Lat/Lon
     *
     * @param lat input double of the latitude
     * @param lon input double of the longitude
     * @since 1.0.0
     */
    private void animateToLoc(double lat, double lon) {
        GoogleMap googleMap = getTheDamnMap();
        LatLng LL = new LatLng(lat, lon);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LL, 17));
    }

    /**
     * This function will animate the map to an input LatLng
     *
     * @param LL LatLng
     * @since 1.0.0
     */
    @SuppressWarnings("unused")
    private void animateToLoc(LatLng LL) {
        GoogleMap googleMap = getTheDamnMap();
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LL, 17));
    }

    /**
     * This method will save all markers present on the screen (excluding my position and distance markers)
     * to a text file "savedmarkers.jml" in the downloads folder.
     *
     * @since 1.0.0
     */
    public void saveAllMarkers() {
        //int drawable, String title, String snippit, LatLng latLng
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<MarkerFile>");
        int goodMarkers = myMarkerStore.length;
        for (int i = myMarkerStore.length - 1; i > 0; i--) {
            if (D) Log.d("MarkersNum", goodMarkers + "");
            //  if(myMarkerStore[i].getTitle().matches(""))goodMarkers--;
            if (myMarkerStore[i] == null) goodMarkers--;
            if (myMarkerStore[i] != null) {
                if (!myMarkerStore[i].getTitle().matches("My Position")) {
                    stringBuilder.append("<Marker>");
                    if (D) Log.d("GoodMarker " + i, "LL " + myMarkerStore[i].getLatLng().toString());
                    stringBuilder.append("<LatLng>").append(myMarkerStore[i].getLatLng()).append("<EndLatLng>");
                    if (D) Log.d("GoodMarker " + i, "Icon " + myMarkerStore[i].getMarkerStr()); //int
                    stringBuilder.append("<Icon>").append(myMarkerStore[i].getMarkerStr()).append("<EndIcon>");
                    if (D) Log.d("GoodMarker " + i, "Title " + myMarkerStore[i].getTitle());
                    stringBuilder.append("<Title>").append(myMarkerStore[i].getTitle()).append("<EndTitle>");
                    if (D) Log.d("GoodMarker " + i, "Snip " + myMarkerStore[i].getSnippit());
                    stringBuilder.append("<Snippit>").append(myMarkerStore[i].getSnippit()).append("<EndSnippit>");
                    stringBuilder.append("<EndMarker>");
                }
            }
        }
        stringBuilder.append("<EndMarkerFile>");
        writeToDownloadDirectory("savedmarkers.jml", stringBuilder.toString());
        doAToast("Markers Saved", 1);
    }

    /**
     * This method will redraw all markers from a saved markers file "savedmarkers.jml".
     *
     * @param filename Currently not in use but could be used to provide redrawing from different files.
     * @since 1.0.0
     */
    @SuppressWarnings("unused")
    private void reinstateMarkersFromFile(String filename) {

        String returnedMarkers = readFromDownloadDirectory("savedmarkers.jml");
        if (!returnedMarkers.matches("nope")) {

            if (D) Log.d("Returned Markers", returnedMarkers);
            int i = 0;
            while (returnedMarkers.contains("<MarkerFile><Marker>")) {

                String lastMarker = returnedMarkers.substring(returnedMarkers.lastIndexOf("<Marker>"), returnedMarkers.lastIndexOf("<EndMarker>"));
                returnedMarkers = returnedMarkers.substring(0, returnedMarkers.lastIndexOf("<Marker>"));
                if (D) Log.d("Last Marker", lastMarker);
                if (D) Log.d("New Returned Markers " + i, returnedMarkers);
                String markerLLStr = lastMarker.substring(lastMarker.indexOf("<LatLng>") + 8, lastMarker.indexOf("<EndLatLng>"));
                if (D) Log.d("NewMarker " + i, "LLSTR = " + markerLLStr);
                int indexOpen = markerLLStr.indexOf("(");
                int indexComma = markerLLStr.indexOf(",");
                int indexClose = markerLLStr.indexOf(")");
                String latStr = markerLLStr.substring(indexOpen + 1, indexComma);
                String lonStr = markerLLStr.substring(indexComma + 1, indexClose);
                if (D) Log.d("NewMarker " + i, "latStr=" + latStr);
                if (D) Log.d("NewMarker " + i, "lonStr=" + lonStr);
                LatLng markerLL = new LatLng(parseDouble(latStr), parseDouble(lonStr));
                String markerTitle = lastMarker.substring(lastMarker.indexOf("<Title>") + 7, lastMarker.indexOf("<EndTitle>"));
                if (D) Log.d("NewMarker " + i, "Title = " + markerTitle);
                String markerSnippit = lastMarker.substring(lastMarker.indexOf("<Snippit>") + 9, lastMarker.indexOf("<EndSnippit>"));
                if (D) Log.d("NewMarker " + i, "Snippit = " + markerSnippit);
                String markerIconTitle = lastMarker.substring(lastMarker.indexOf("<Icon>") + 6, lastMarker.indexOf("<EndIcon>"));
                if (D) Log.d("NewMarker " + i, "Icon = " + markerIconTitle);
                SYMBOLDICT symboldict = SYMBOLDICT.valueOf(markerIconTitle);
                makeNewMarker(symboldict.drawable, markerTitle, markerSnippit, markerLL);

                i++;
            }
            doAToast("Markers Restored", 1);
        }
    }

    /**
     * This will read a file from the downloads directory and return the content of file as a string.
     *
     * @param filename file which you want to be retrieved. A string is expected. Ex) "file.txt"
     * @return the contents of the file as a String
     * @since 1.0.0
     */
    private String readFromDownloadDirectory(String filename) {
        File[] downloadFiles = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).listFiles();
        File goodFile = null;

        for (int i = 0; i < downloadFiles.length; i++) {
            if (downloadFiles[i].getName().matches(filename)) {
                goodFile = downloadFiles[i];
            }
        }
        if (goodFile == null) {
            doAToast("File " + filename + " not found", 1);
            return "nope";
        }
        //Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(goodFile));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }

    /**
     * This will write a file to the downloads directory, file name and data provided at use.
     *
     * @param filename String of the filename to write, Ex) "file.txt"
     * @param data     String of the physical data to put into the new file.
     * @since 1.0.0
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void writeToDownloadDirectory(String filename, String data) {

        File[] downloadFiles = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).listFiles();

        for (int i = 0; i < downloadFiles.length; i++) {
            if (downloadFiles[i].getName().matches(filename)) {
                File goodFile = downloadFiles[i];
                goodFile.delete();
            }
        }


        File myFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + filename);
        try {
            myFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(myFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        OutputStreamWriter myOutWriter = new OutputStreamWriter(
                fOut);
        try {
            myOutWriter.append(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            myOutWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (fOut != null) {
                fOut.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method saveLastPosition() saves CURRENT_LAT and CURRENT_LON global variables
     *
     * @since 1.0.0
     */
    private void saveLastPosition() {

        writeIntoInternalStorage(getString(R.string.latFN), Double.toString(Global.CURRENT_LAT));
        if (D) Log.d(debugTag, "lastLat = " + Global.CURRENT_LAT);
        writeIntoInternalStorage(getString(R.string.lonFN), Double.toString(Global.CURRENT_LON));
        if (D) Log.d(debugTag, "lastLon = " + Global.CURRENT_LON);
        writeIntoInternalStorage(getString(R.string.altFN), Double.toString(Global.CURRENT_ALT));

        //myMarkerStore[1].
    }

    /**
     * Method restorePreviousPosition() restores global variables CURRENT_LAT and CURRENT_LON
     * to last saved values.
     * If the files are not found we will default to Rowan Hall
     *
     * @since 1.0.0
     */
    private void restorePreviousPosition() throws IOException {
        if (checkForFile(getString(R.string.latFN))) {
            Global.CURRENT_LAT = parseDouble(returnInternalStorageInfo(getString(R.string.latFN)));
        } else {
            writeIntoInternalStorage(getString(R.string.latFN), "39.71224");
        }
        if (checkForFile(getString(R.string.lonFN))) {
            Global.CURRENT_LON = parseDouble(returnInternalStorageInfo(getString(R.string.lonFN)));
        } else {
            writeIntoInternalStorage(getString(R.string.lonFN), "-75.1222");
            restorePreviousPosition();
        }
        if (D) Log.d(debugTag, "Position restored");
    }

    /**
     * Given 2 LatLng (OpenMap) points, this will return a double of the distance between the 2 points.
     *
     * @param ll1 Input LatLng point 1
     * @param ll2 Input LatLng point 2
     * @return double distance between the two input points.
     * @since 1.0.0
     */
    private double distanceBetweenCoords(LatLng ll1, LatLng ll2) {
        //1 miles   2 KM
        String distUnit = sharedPref.getString("prefUnitsFormat", "1");

        /*The formula uses the distance calculation on a sphere:
        e = ARCCOS[ SIN(LAT1)*SIN(LAT2) + COS(LAT1)*COS(LAT2)*COS(LONG2-LONG1) ]
        e = ARCCOS[ SIN(50.11222)*SIN(52.52222) + COS(50.11222)*COS(52.52222)*COS(13.29750-8.68194) ]
        e = ARCCOS[ 0.60892 + 0.38893 ]
        e = 3.75781 / 180 * PI = 0.06559

        Now, the obtained result must be multiplied with the radius of the equator:
        Distance = e * r = 0.06559 * 6378.137 km = 418.34 km  or
        Distance = e * r = 0.06559 * 3963.191 miles = 259.95 miles */

   /*     double pi = 3.14159265359;
        double radiusEarthMiles = 3963.191;
        double radiusEarthKM = 6378.137;
        double distance;

        double distance1 = StrictMath.acos(StrictMath.sin(ll1.latitude) * StrictMath.sin(ll2.latitude)
                + StrictMath.cos(ll1.latitude) * StrictMath.cos(ll2.latitude) * StrictMath.cos(ll2.longitude - ll1.longitude));
        double distance2 = distance1 / 180 * pi;



        return distance;*/                    //   old way
        distanceBetweenCoordsActive = false;
        distanceBetweenCoordsNum = 0;
        if (distUnit.matches("1")) {
            return (SphericalUtil.computeDistanceBetween(ll1, ll2) / 1000) * 0.621371192237334;
        } else {
            return SphericalUtil.computeDistanceBetween(ll1, ll2) / 1000;     //km
        }

    }

    /**
     * onPause functionality, this happens when the user hits the home button or the app is closed somehow.
     *
     * @since 1.0.0
     */
    @Override
    public void onPause() {
        super.onPause();
        if (D) Log.d(debugTag, "onPause Hit");
        saveLastPosition();
        Global.lastPosLL = new LatLng(Global.CURRENT_LAT, Global.CURRENT_LON);
        //  Global.CURRENT_LAT = 0;
        //  Global.CURRENT_LON = 0;
        //turn off the gps so we don't murder the battery
        if (!whereIveBeenOn) {
            findCurrentGPSLocation.onPause();
            gpsON = false;
        }
        if (lightOn) {
            flashlight.setTorch(false);
            flashlight.stop();
            lightOn = false;
        }
    }

    /**
     * onResume functionality. this happens when the app is started and/or resumed from being paused.
     * Make sure that the functionality doesn't depend on something running because we cant guarantee that
     * it is already when we start the app. IF we need something to be running like the GPS check somehow.
     *
     * @since 1.0.0
     */
    @Override
    public void onResume() {
        super.onResume();
        if (D) Log.d(debugTag, "onResume triggered");

        //restore the default symbol to the sidebar.
        if (checkForFile(getString(R.string.defaultSymbolFN))) {
            try {
                ibSidebarMain.setImageResource(returnSymbolFromString(returnInternalStorageInfo(getString(R.string.defaultSymbolFN))).drawable);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //If we have a last position, turn on sidebar and go to markers option
        if (Global.lastPosLL != null) {
            //animate to the last location
            //TODO:READ OPTION TO SEE IF USER WANTS TO GO TO THAT POS
            //if (Global.lastPosLL.longitude != 0) animateToLoc(Global.lastPosLL);

            //open the sidebar and enable marker options to fix a bug where map types does not appear
            if (!sidebarOpen) toggleSideBar();
            enableMarkersOptions();
        }

        //Turn minimap on or off
        FragmentManager fm = getFragmentManager();
        MapFragment mapFrag = (MapFragment) fm.findFragmentById(R.id.miniMap);
        if (sharedPref.getBoolean("showMiniMap", true)) {
            mapFrag.getView().setVisibility(View.VISIBLE);
        } else {
            mapFrag.getView().setVisibility(View.GONE);
        }

        //check to see if there was a GPS location from the last time the app ran
        try {
            if (checkForFile(getString(R.string.latFN))) {
                try {
                    //found the location text files
                    restorePreviousPosition();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                //didnt find the location textfiles, make them.
                writeIntoInternalStorage(getString(R.string.latFN), Double.toString(0));
                writeIntoInternalStorage(getString(R.string.lonFN), Double.toString(0));
                writeIntoInternalStorage(getString(R.string.altFN), Double.toString(0));
                restorePreviousPosition();
            }

        } catch (IOException e) {
            e.printStackTrace();
            doAToast("Previous location not found", 1);
            Global.CURRENT_LAT = 0;
            Global.CURRENT_LON = 0;
        }

        //turn the gps back on
        if (!gpsON) {
            findCurrentGPSLocation.onResume();
        }
    }

    /**
     * onDestroy functionality. this happens if hte app is forced closed or is truly exited.
     *
     * @since 1.0.0
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        saveLastPosition();
        if (lightOn) {
            flashlight.setTorch(false);
            flashlight.stop();
            lightOn = false;
        }
    }

    private String updateWifiList() {

        scanResultList = wifiManager.getScanResults();
        Log.wtf("FirstActivity", scanResultList.toString());
        numResults = scanResultList.size();
        wifiReceiver = new WifiScanReceiver(this);
        wifiManager.startScan();

        List<ScanResult> wifiScanList = wifiManager.getScanResults();
        ssidList = new String[wifiScanList.size()];
        ArrayList<String> ssids = new ArrayList<String>();

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < wifiScanList.size(); i++) {
            ssidList[i] = ((wifiScanList.get(i)).SSID);
            ssids.add(wifiScanList.get(i).SSID + "..." + wifiScanList.get(i).level);
            stringBuilder.append("\n" + wifiScanList.get(i).SSID + "..." + wifiScanList.get(i).level);
            Log.wtf("FirstActivity", "adding..." + wifiScanList.get(i).SSID);
        }
        return stringBuilder.toString();

    }

    //////////////////////////////////////////////////////////////////////////////
    //start of Andy's Code

    private void writeWifiList() {
        if (Global.CURRENT_MGRS != null) {
            writeToDownloadDirectory("WifiScan_" + Global.CURRENT_MGRS, updateWifiList());
        } else {
            writeToDownloadDirectory("WifiScan_MISSINGLOC_" + Global.CURRENT_MGRS, updateWifiList());

        }
    }

    private void updateMarkerGroups() {
//        float zoomLevel = getTheDamnMap().getCameraPosition().zoom;
        //find all the markers that are currently existing
        //assign markers into a vector that will correspond to myMarkerStore

        myMarkerPixelCoord = new Vector<Point>();      //jr initialized
        int height = this.getWindowManager().getDefaultDisplay().getWidth();
        int width = this.getWindowManager().getDefaultDisplay().getHeight();

        int numContainers = 0;   //jr

        for (int i = 0; i < Global.numMarkers; i++) {      //jr changed from 1 to 0
            Marker marker = myMarkerStore[i].getMarker();
            myMarkerPixelCoord.add(i, convertLatLongToPixel(marker, height, width));
            if (marker.getTitle().matches("Marker Container") && isOnScreen(marker.getPosition())) {      //jr
                numContainers++;                                        //jr
            }                                                               //jr
        }
        Log.wtf("FirstActivity2470", numContainers + " Containers found");          //jr

        //compare pixel distance between markers
        //iterate through 1 to size of myMarkerPixelCoord, i
        //iterate again starting at i+1, skipping i, and going to size,j

        for (int i = 1; i < myMarkerPixelCoord.size(); i++) {
            for (int j = i + 1; j < myMarkerPixelCoord.size(); j++) {
                if (calculatePixelDistance(myMarkerPixelCoord.get(i), myMarkerPixelCoord.get(j)) > pixelDistanceToMerge) {
                    //do whatever to store pairs
                }
            }
        }
        //ScreenCoordinates screenCoordinates = new ScreenCoordinates();
        isGridManagerRunning = true;
        myAsyncTask = new MyAsyncTask();
        myAsyncTask.execute();


    }

    //jr
    private boolean isOnScreen(LatLng latLng) {
        //ScreenCoordinates bigMapCoords = new ScreenCoordinates();
        return ((isBetween(latLng.latitude, ScreenCoordinates.topLeft.latitude, ScreenCoordinates.bottomRight.latitude))
                && (isBetween(latLng.longitude, ScreenCoordinates.topLeft.longitude, ScreenCoordinates.bottomRight.longitude)));
    }

    //jr
    private boolean isBetween(double subject, double top, double bottom) {
        return ((subject >= bottom && subject <= top) || (subject <= bottom && subject >= top));
    }

    private Point convertLatLongToPixel(Marker marker, int height, int width) {
        //float zoomLevel = getTheDamnMap().getCameraPosition().zoom;
        //long = x, lat = y
        double xPixel, yPixel;
        double latSize, lngSize;
        //Point point= new Point(height,width);
        //ScreenCoordinates coord = new ScreenCoordinates();
        lngSize = Math.abs(ScreenCoordinates.topLeft.longitude - ScreenCoordinates.bottomRight.longitude);
        latSize = Math.abs(ScreenCoordinates.topLeft.latitude - ScreenCoordinates.bottomRight.latitude);

        xPixel = Math.abs(height * (ScreenCoordinates.bottomRight.longitude - marker.getPosition().longitude) / lngSize);
        yPixel = Math.abs(width * (ScreenCoordinates.bottomRight.latitude - marker.getPosition().latitude) / latSize);


        return new Point((int) xPixel, (int) yPixel);

    }

    private int calculatePixelDistance(Point point1, Point point2) {
        double distance;
        distance = Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
        return (int) distance;
    }

    /**
     * Enumeration to provide a reference for which sidebar layout is currently selected.
     *
     * @since 1.0.0
     */
    public enum SIDEBARLAYOUTTYPE {
        /**
         * No layout is selected for sidebar. this should never be used.
         */
        NONE("NoLayout", 0),
        /**
         * Map types layout is selected.
         */
        MAPTYPES("MapTypes", 1),
        /**
         * Map layers is selected
         * Currently unimplemented
         */
        MAPLAYERS("MapLayers", 2),
        /**
         * Map markers is selected
         */
        MAPMARKERS("MapMarkers", 3),

        MAPDISTANCE("MapDistance", 4);

        /**
         * The string value of the current sidebar layout
         */
        private String stringVal;
        /**
         * The integer value of the current sidebar layout
         */
        private int intVal;

        private SIDEBARLAYOUTTYPE(String toString, int value) {
            stringVal = toString;
            intVal = value;
        }

        /**
         * overrides the normal toString functionality
         *
         * @return the string value of the requested sidebar layout type
         */
        @Override
        public String toString() {
            return stringVal;
        }
    }

    /**
     * Enumeration to provide a reference from the marker titles (strings) to the images (drawables).
     *
     * @since 1.0.0
     */
    public enum SYMBOLDICT {
        FriendlyGround("FriendlyGround", R.drawable.friend_ground),
        FriendlySeaSurface("FriendlySeaSurface", R.drawable.friend_seasurface),
        FriendlyAirSpace("FriendlyAirSpace", R.drawable.friend_airspace),
        FriendlySubsurface("FriendlySubsurface", R.drawable.friend_subsurface),
        HostileGround("HostileGround", R.drawable.hostile_ground),
        HostileSeaSurface("HostileSeaSurface", R.drawable.hostile_seasurface),
        HostileAirSpace("HostileAirSpace", R.drawable.hostile_airspace),
        HostileSubsurface("HostileSubsurface", R.drawable.hostile_subsurface),
        NeutralGround("NeutralGround", R.drawable.neutral_ground),
        NeutralSeaSurface("NeutralSeaSurface", R.drawable.neutral_seasurface),
        NeutralAirSpace("NeutralAirSpace", R.drawable.neutral_airspace),
        NeutralSubsurface("NeutralSubsurface", R.drawable.neutral_subsurface),
        UnknownGround("UnknownGround", R.drawable.unknown_ground),
        UnknownSeaSurface("UnknownSeaSurface", R.drawable.unknown_seasurface),
        UnknownAirSpace("UnknownAirSpace", R.drawable.unknown_airspace),
        UnknownSubsurface("UnknownSubsurface", R.drawable.unknown_subsurface);

        public String stringVal;
        public int drawable;

        private SYMBOLDICT(String toString, int drawabl) {
            stringVal = toString;
            drawable = drawabl;
        }

        @Override
        public String toString() {
            return stringVal;
        }
    }

    /**
     * Thread to control the GPS operations
     *
     * @since 1.0.0
     */
    private class FindCurrentGPSLocation extends Thread {

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /**
         * Constructor
         */
        public FindCurrentGPSLocation() {

        }        //Location listener...this is where we control how the GPS is handled

        LocationListener locationListener = new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
                // called when the location provider status changes. Possible status: OUT_OF_SERVICE, TEMPORARILY_UNAVAILABLE or AVAILABLE.
                if (T) doAToast("onStatusChanged", 0);

            }

            public void onProviderEnabled(String provider) {
                // called when the location provider is enabled by the user
                if (T) doAToast("onProviderEnabled", 0);
            }

            public void onProviderDisabled(String provider) {
                // called when the location provider is disabled by the user. If it is already disabled, it's called immediately after requestLocationUpdates
                if (T) doAToast("onProviderDisabled", 0);
            }

            //When the user moves this runs and the GPS variables are updated.
            public void onLocationChanged(Location location) {

                //Update Lat Long Current Location to new location
                updateLLfromGPS(location);

                //Update MGRS Current Location to new location
                updateMGRS();

                //Update Altitudes
                //TODO: Doesnt work well
                cameraAlt = location.getAltitude() + 1500;//add 1500 meters to view your current alt  well.
                Global.CURRENT_ALT = location.getAltitude() + 1500;


                if (whereIveBeenOn) {
                    LatLng latln = new LatLng(location.getLatitude(), location.getLongitude());
                    List<LatLng> pts = whereIveBeenPL.getPoints();
                    pts.add(latln);
                    whereIveBeenPL.setPoints(pts);

                    if (whereIveBeenLastPoint == null && whereIveBeenNewPoint != null) {
                        whereIveBeenLastPoint = whereIveBeenNewPoint;
                        whereIveBeenNewPoint = latln;
                    } else if (whereIveBeenNewPoint == null && whereIveBeenLastPoint == null) {
                        whereIveBeenDistance = 0;
                        whereIveBeenNewPoint = latln;
                    } else {
                        double dist = distanceBetweenCoords(whereIveBeenLastPoint, whereIveBeenNewPoint);
                        if (!Double.isNaN(dist)) {
                            whereIveBeenDistance += dist;
                            if (distanceBetweenCoords(whereIveBeenLastPoint, whereIveBeenNewPoint) > .2)
                                doAToast("Distance greater then .2 Mi", 1);
                            Log.wtf("GPSTESTER 0", "distance difference = " + distanceBetweenCoords(whereIveBeenLastPoint, whereIveBeenNewPoint));
                            Log.wtf("GPSTESTER 1", "distance difference(string) = " + Double.toString(distanceBetweenCoords(whereIveBeenLastPoint, whereIveBeenNewPoint)));
                            whereIveBeenLastPoint = whereIveBeenNewPoint;
                            whereIveBeenNewPoint = latln;
                            TextView tv = (TextView) findViewById(R.id.distanceTraveledTV);
                            //you need a fudge factor because the stupid gps isnt as accurate as one might think. .83-.88 seems to be ideal      .99 w/ new calculations
                            double gpsFudgeFactor = Double.parseDouble(sharedPref.getString("gpsFudge", "0.99"));
                            tv.setText("" + roundTo3DecimalPlaces(whereIveBeenDistance * gpsFudgeFactor) + " Miles");
                            Log.wtf("GPSTESTER 2", "New Lat/Lon=" + latln.toString());
                            Log.wtf("GPSTESTER 3", "New Dist=" + whereIveBeenDistance);
                            Log.wtf("GPSTESTER 4", "New Dist to string =" + Double.toString(whereIveBeenDistance));
                        }
                    }


                }
            }
        };

        /**
         * Run the GPS thread. This will add the location listener to the location manager
         */
        public void run() {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }

        /**
         * Turn off the gps when the app is paused to avoid unnecessary draining of battery
         */
        public void onPause() {
            if (!whereIveBeenOn) {
                locationManager.removeUpdates(locationListener);
                if (D) Log.d("FirstActivity GPS Class", "gps paused");
            }
        }

        /**
         * Turn the gps back on when the app is resumed so we know where we are
         */
        public void onResume() {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }


        //end gps class
    }

    public class MyAsyncTask extends AsyncTask<String, Void, String> {

        //ScreenCoordinates screenCoordinates;

    	/*public MyAsyncTask(ScreenCoordinates screenCoordinates) {
            //this.screenCoordinates = screenCoordinates;
        }*/

        @Override
        protected String doInBackground(String... params) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    isGridManagerRunning = true;
                    
                    // What is this?
                    /*
                    int numOfIterations = 200;
                    double difference = ScreenCoordinates.topLeft.latitude - ScreenCoordinates.bottomLeft.latitude;

                    for (int i = 1; i <= numOfIterations; i++) {
                        LatLng latLng = new LatLng(ScreenCoordinates.bottomLeft.latitude + ((difference / numOfIterations) * i), ScreenCoordinates.bottomLeft.longitude);
                        Log.wtf(debugTag, "MGRS: " + Coordinates.convertLL_to_MGRS(new LatLonPoint(latLng.latitude, latLng.longitude)));
                    }*/

                }
            });

            thread.start();
            return "";
        }

        @Override
        protected void onProgressUpdate(Void... progress) {
            //Might as well just leave this blank. You don't need to show anything whenever the map is touched
        }

        @Override
        protected void onPostExecute(String result) {
            Log.wtf(debugTag, "done with grid calcs");
            //Do some jawns that needs to be done after code executes. Change void if you need to return something
        }

        @Override
        protected void onCancelled() {
            Log.wtf(debugTag, "grid calcs cancelled");
        }
    }
    //end of andy code
    //////////////////////////////////////////////////////////////////////////////
}
