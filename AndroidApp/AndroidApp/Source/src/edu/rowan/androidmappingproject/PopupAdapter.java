package edu.rowan.androidmappingproject;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 3/17/13
 * <br>
 * <br>
 */
class PopupAdapter implements GoogleMap.InfoWindowAdapter {
    LayoutInflater inflater = null;
    private boolean D = false;

    PopupAdapter(LayoutInflater inflater) {
        this.inflater = inflater;

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return (null);
    }

    @Override
    public View getInfoContents(final Marker marker) {
        View popup = inflater.inflate(R.layout.popup, null);
        TextView tv = (TextView) popup.findViewById(R.id.title);
        if (D) Log.wtf("PopupAdapter 36 ", marker.getTitle());


        String markID = marker.getId();
        int markerID;
        try {
            if (D) Log.wtf("PopupAdapter 41 ", markID);
            if (marker.getTitle().contains("My Position")) {
                markerID = Global.showMyPOSMarkerID;
            } else {
                markerID = Integer.parseInt(markID.substring(1));
            }
            if (D) Log.wtf("PopupAdapter 49 ", String.valueOf(markerID));
            if (D) Log.wtf("PopupAdapter 50 ", FirstActivity.myMarkerStore[markerID].getSnippit());
            tv.setText(FirstActivity.myMarkerStore[markerID].getTitle());
            tv.setTextColor(Color.BLACK);

            tv = (TextView) popup.findViewById(R.id.snippet);

            tv.setText(FirstActivity.myMarkerStore[markerID].getSnippit());
            tv.setTextColor(Color.BLACK);
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }

        return (popup);
    }
}