package edu.rowan.androidmappingproject;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 4/4/13
 * <br>
 * <br>
 */


public class MapTypes {

    static TileOverlay overlay = null;

    /**
     * Utility function because I got tired of writing these 3 lines in every function and it was late.
     *
     * @return the GoogleMap from FragmentManager.MapFragment.getMap()
     */
    private static GoogleMap getTheDamnMap() {
        return Global.gmap;
    }

    /**
     * This function will change the current map type to satellite map
     */
    public static void enableSatMap() {
        disableMap();
        GoogleMap googleMap = getTheDamnMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }

    /**
     * This function will change the current map type to the hybrid map
     */
    public static void enableHybridMap() {
        disableMap();
        GoogleMap googleMap = getTheDamnMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    /**
     * This function will change the current map type to normal map
     */
    public static void enableNormalMap() {
        disableMap();
        GoogleMap googleMap = getTheDamnMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    /**
     * This function will change the current map type to the terrain map.
     * Zoom levels are greatly decreased at this level.
     */
    public static void enableTerrainMap() {
        disableMap();
        GoogleMap googleMap = getTheDamnMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    /**
     * This function will change the current map type to the terrain map.
     * Zoom levels are greatly decreased at this level.
     */
    public static void enableCustomMap() {
        disableMap();
        GoogleMap googleMap = getTheDamnMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
        overlay = googleMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomTileProvider()));
    }

    /**
     * This function will disable the showing of any built in map. The markers and such will still be visible.
     */
    public static void disableMap() {
        GoogleMap googleMap = getTheDamnMap();
        googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

        if (overlay != null)
            overlay.remove();
    }


}
