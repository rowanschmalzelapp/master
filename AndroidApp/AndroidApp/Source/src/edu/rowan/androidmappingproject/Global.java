package edu.rowan.androidmappingproject;/*
 * Copyright (c) 2012. Rowan University Center for Sustainable Design. File created by Justin Reda on 12.8.2012. Unauthorized use of this code, duplication, or modification prohibited.
 */

import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by IntelliJ IDEA.
 * User: Justin
 * Date: 8/12/12
 * Time: 8:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class Global {
    public static double CURRENT_LAT = 0;
    public static double CURRENT_LON = 0;
    public static double CURRENT_ALT = 0;
    public static String CURRENT_MGRS = "";

    public static int currentSideBarLayout = 0;

    public static Button goToButton;
    public static Button dropPinButton;

    public static TextView mgrsCoordsField;
    public static TextView latCoordsField;
    public static TextView lonCoordsField;
    public static TextView northingCoordsField;
    public static TextView eastingCoordsField;
    public static TextView zoneNumberField;
    public static TextView zoneLetterField;

    public static EditText mgrsEnterCoordsField;
    public static EditText latEnterCoordsField;
    public static EditText lonEnterCoordsField;
    public static EditText northingEnterCoordsField;
    public static EditText eastingEnterCoordsField;
    public static EditText zoneNumberEnterField;
    public static EditText zoneLetterEnterField;

    public static FirstActivity.SYMBOLDICT currentSymbol = FirstActivity.SYMBOLDICT.FriendlyGround;
    public static String myLocMarkerId = "";

    public static int numMarkers = 0;
    public static int showMyPOSMarkerID = 999;

    public static GoogleMap gmap = null;

    public static LatLng lastPosLL = null;

    public static Marker CURRENTPOSMARKER;

    public static Marker dropLocationMarker1;
    public static Marker dropLocationMarker2;

    public static int dLMarker1ID = 0;
    public static int dLMarker2ID = 0;

    public static Context context;

    public static MultipleMarkerContainer testMMC;
    public static MultipleMarkerContainer[] multipleMarkerContainers;

}
