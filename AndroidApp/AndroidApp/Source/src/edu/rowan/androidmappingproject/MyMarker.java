package edu.rowan.androidmappingproject;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 4/4/13
 * <br>
 * <br>
 */
public class MyMarker {
    private LatLng latLng = new LatLng(0, 0);
    private String title = "";
    private String snippit = "";
    private int icon = R.drawable.friend_ground;
    private int id = -1;

    public String getMarkerStr() {
        return markerStr;
    }

    public void setMarkerStr(String markerStr) {
        this.markerStr = markerStr;
    }

    private String markerStr = null;
    private Marker marker;

/*    public void dropMarker(GoogleMap googleMap){
        FirstActivity.makeNewMarker();
    }*/


    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }


    public int getSnippitID() {
        return snippitID;
    }

    public void setSnippitID(int snippitID) {
        this.snippitID = snippitID;
    }

    private int snippitID = 0;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSnippit() {
        return snippit;
    }

    public void setSnippit(String snippit) {
        this.snippit = snippit;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

}
