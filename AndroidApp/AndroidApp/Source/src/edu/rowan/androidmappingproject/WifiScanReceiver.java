package edu.rowan.androidmappingproject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Justin
 * Date: 1/29/14
 * Time: 2:58 AM
 * To change this template use File | Settings | File Templates.
 */
class WifiScanReceiver extends BroadcastReceiver {
    private FirstActivity firstActivity;

    public WifiScanReceiver(FirstActivity firstActivity) {
        this.firstActivity = firstActivity;
    }

    public void onReceive(Context c, Intent intent) {
        List<ScanResult> wifiScanList = firstActivity.wifiManager.getScanResults();
        firstActivity.ssidList = new String[wifiScanList.size()];
        for (int i = 0; i < wifiScanList.size(); i++) {
            firstActivity.ssidList[i] = ((wifiScanList.get(i)).toString());
        }

    }
}
