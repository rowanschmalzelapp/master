package edu.rowan.androidmappingproject;

import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Justin on 12/5/13.
 */
public class MultipleMarkerContainer {

    private int maxMarkersPerContainer = 99;
    private MyMarker[] markerArray;// = new MyMarker[maxMarkersPerContainer];
    private String markerContainerTitle = "Container";
    private String markerContainerSnippit = "ContainerSnippit";
    private LatLng markerContainerLatLng = new LatLng(0, 0);
    private int markerContainerIcon = R.drawable.distance_marker_green_pin;
    private Marker thisMarker;
    private int markersInArray = 0;

    public MultipleMarkerContainer() {
    }

    public Marker getThisMarker() {
        return thisMarker;
    }

    public void setThisMarker(Marker thisMarker) {
        this.thisMarker = thisMarker;
    }

    public int getMarkersInArray() {
        return markersInArray;
    }

    public void setMarkersInArray(int markersInArray) {
        this.markersInArray = markersInArray;
    }

    public MyMarker[] setMarkerInArray(MyMarker[] oldMarkerArray, int positionInArray, MyMarker markerToAdd) {
        oldMarkerArray[positionInArray] = markerToAdd;
        return oldMarkerArray;
    }

    public MyMarker[] addMarkerToArray(MyMarker[] oldMarkerArray, MyMarker markerToAdd) {
        setMarkersInArray(getMarkersInArray() + 1);
        markerToAdd.getMarker().setVisible(false);
        return setMarkerInArray(oldMarkerArray, markersInArray - 1, markerToAdd);
    }

    public int getMaxMarkersPerContainer() {
        return maxMarkersPerContainer;
    }

    public void setMaxMarkersPerContainer(int maxMarkersPerContainer) {
        this.maxMarkersPerContainer = maxMarkersPerContainer;
    }

    public MyMarker[] getMarkerArray() {
        return markerArray;
    }

    public void setMarkerArray(MyMarker[] markerArray) {
        this.markerArray = markerArray;
    }

    public String getMarkerContainerTitle() {
        return markerContainerTitle;
    }

    public void setMarkerContainerTitle(String markerContainerTitle) {
        this.markerContainerTitle = markerContainerTitle;
    }

    public String getMarkerContainerSnippit() {
        return markerContainerSnippit;
    }

    public void setMarkerContainerSnippit(String markerContainerSnippit) {
        this.markerContainerSnippit = markerContainerSnippit;
    }

    public LatLng getMarkerContainerLatLng() {
        return markerContainerLatLng;
    }

    public void setMarkerContainerLatLng(LatLng markerContainerLatLng) {
        this.markerContainerLatLng = markerContainerLatLng;
    }

    public int getMarkerContainerIcon() {
        return markerContainerIcon;
    }

    public void setMarkerContainerIcon(int markerContainerIcon) {
        this.markerContainerIcon = markerContainerIcon;
    }

    public void expandIntoOriginal() {
        Log.wtf("MultipleMarkerContainer", "Expand into original" + getMarkersInArray() + "Markers");
        int i = 0;
        for (i = 0; i < getMarkersInArray(); i++) {
            //getMarkerArray()[i];
            getMarkerArray()[i].getMarker();
            getMarkerArray()[i].getMarker().getId();
            String markID = getMarkerArray()[i].getMarker().getId();
            Log.wtf("MultipleMarkerContainer", "id: " + markID);
            int markerID;
            markerID = Integer.parseInt(markID.substring(1));
            FirstActivity.myMarkerStore[markerID].getMarker().setVisible(true);
        }
        Log.wtf("MultipleMarkerContainer", i + " Markers should have been restored");

    }


}
