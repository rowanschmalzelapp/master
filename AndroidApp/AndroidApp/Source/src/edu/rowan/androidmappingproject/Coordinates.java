package edu.rowan.androidmappingproject;

import android.widget.EditText;
import android_OpenMap_Framework.common.LatLonPoint;
import android_OpenMap_Framework.common.UTMPoint;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 4/4/13
 * <br>
 * <br>
 * This class contains all functionality related to coordinate conversions.
 */
public class Coordinates {

    /**
     * Convert a MGRS string into a LatLonPoint. LatLonPoint is from the OpenMapAPI
     *
     * @param mgrsIN String of MGRS coordinate.
     * @return a LatLonPoint which can be parsed into individual parameters.
     */
    public static LatLonPoint convertMGRS_to_LatLonPoint(String mgrsIN) {

        android_OpenMap_Framework.common.MGRSPoint MGRS_Point = new android_OpenMap_Framework.common.MGRSPoint(mgrsIN);

        return MGRS_Point.toLatLonPoint();
    }

    /**
     * Convert a UTM point into a LatLonPoint. the UTM point must be input as its separate components.
     *
     * @param northing    double of the northing of the input UTM point.
     * @param easting     double of the easting of the input UTM point.
     * @param zone_number int of the zone number from the input UTM point.
     * @param zone_letter char of the zone letter from the input UTM point.
     * @return a LatLongPoint that has been converted from UTM. LatLonPoint is part of OpenMapAPI
     */
    public static LatLonPoint convertUTM_to_LatLonPoint(double northing, double easting, int zone_number, char zone_letter) {

        android_OpenMap_Framework.common.UTMPoint UTM_Point = new android_OpenMap_Framework.common.UTMPoint(northing, easting, zone_number, zone_letter);

        return UTM_Point.toLatLonPoint();
    }

    /**
     * Convert a LatLonPoint into a UTM point. The UTM point can be broken up into its components from the return value.
     *
     * @param latLonPoint input LatLonPoint to ne converted. LatLonPoint is from the OpenMapAPI
     * @return a UTM point from the input coordinates. The UTM point can be broken up into its components from this type.
     */
    public static UTMPoint convertLatLon_to_UTM(LatLonPoint latLonPoint) {

        return new UTMPoint(latLonPoint);
    }

    /**
     * Convert a Lat/Lon into an MGRS String
     *
     * @param latLonPoint an input latLonPoint from the GPS or a text field. latLonPoint is from OpenMapAPI
     * @return a String of the MGRS point
     */
    public static String convertLL_to_MGRS(LatLonPoint latLonPoint) {
//        public static String convertLL_to_MGRS(LatLonPoint latLonPoint, int accuracy) {

        //Make a new MGRSPoint from the LatLonPoint and convert it to a string.
        android_OpenMap_Framework.common.MGRSPoint MGRS_Point = new android_OpenMap_Framework.common.MGRSPoint(latLonPoint);

        //makes it crash
        //Set the accuracy of the MGRS Reading.
        // MGRS_Point.setAccuracy(accuracy);

        return MGRS_Point.toString();
    }

    public static LatLngDMS LatLonPoint_to_LatLonDMS(LatLonPoint latLonPoint) {
        LatLngDMS latLngDMS = new LatLngDMS();
        double latIn = latLonPoint.getLatitude();
        double lonIn = latLonPoint.getLongitude();
        // latLngDMS.setDegreesLat(Integer.intValue(latIn))
        String latI = latIn + "";
        String lonI = lonIn + "";

        //parse before decimal
        int decLatLocation = latI.lastIndexOf(".");
        int decLonLocation = lonI.lastIndexOf(".");

        latLngDMS.setDegreesLat(Float.valueOf(latI.substring(0, decLatLocation)));
        latLngDMS.setDegreesLon(Float.valueOf(lonI.substring(0, decLonLocation)));

        double postLat = Double.parseDouble(latI.substring(decLatLocation));
        double postLon = Double.parseDouble(lonI.substring(decLonLocation));
        //make sure i have 0.xxxxx at this point
        postLat /= 100;
        postLon /= 100;

        //  latLngDMS.setMinutesLat((float) (postLat*60));
        //  latLngDMS.setMinutesLon((float) (postLon*60));

        //  latLngDMS.setMinutesLat((float) (postLat*60));
        //  latLngDMS.setMinutesLon((float) (postLon*60));
        return latLngDMS;
    }

    // validMGRS is true if the mgrs string is valid
    // display error messages
    // TODO: validate if symbol is entered
    public static boolean validMGRS(String mgrsString) {
        return android_OpenMap_Framework.common.MGRSPoint.checkValidMGRS(mgrsString);
        /*boolean mgrsValid = true;

        mgrsString = mgrsString.toUpperCase();

        int length = mgrsString.length();

        String hunK = null;
        StringBuffer sb = new StringBuffer();
        char testChar;
        int i = 0;

        if (mgrsString == null || mgrsString.length() == 0) {
            //   if (T) doAToast("Enter MGRS", 1);

            return mgrsValid = false;
        } else {
            while (Character.isLetter(testChar = mgrsString.charAt(i)) == false) {
                if (i >= 2) {
                    // TODO: DOES NOT WORK
                    //           doAToast("Invalid MGRS. \n" + "First two characters need to be a number between 1-60.", 1);

                    return mgrsValid = false;
                }
                sb.append(testChar);
                i++;
            }

            int zone_number = Integer.parseInt(sb.toString());

            if (zone_number < 1 || zone_number > 60) {
                // TODO: DOES NOT WORK
                //        doAToast("Invalid MGRS. \n" + "First two characters need to be a number between 1-60.", 1);

                return mgrsValid = false;
            }

            if (i == 0 || i + 3 > length) {
                // A good MGRS string has to be 4-5 digits long,
                // ##AAA/#AAA at least.
                //       doAToast("Invalid MGRS \n " + "MGRS string must be at least 4-5 digits long", 1);

                return mgrsValid = false;
            }

            char zone_letter = mgrsString.charAt(i++);

            // Should we check the zone letter here? Why not.
            if (zone_letter <= 'A' || zone_letter == 'B' || zone_letter == 'Y' || zone_letter >= 'Z' || zone_letter == 'I' || zone_letter == 'O') {
                //         doAToast("MGRSPoint zone letter " + (char) zone_letter + " not handled: " + mgrsString, 1);

                return mgrsValid = false;
            }

            hunK = mgrsString.substring(i, i += 2);

            // Validate, check the zone, make sure each letter is between A-Z, not I
            // or O
            char char1 = hunK.charAt(0);
            char char2 = hunK.charAt(1);
            if (char1 < 'A' || char2 < 'A' || char1 > 'Z' || char2 > 'Z' || char1 == 'I' || char2 == 'I' || char1 == 'O' || char2 == 'O') {
                //          doAToast("Invalid MGRS. \n" + "Invalid 100k designator", 1);

                return mgrsValid = false;
            }

            // calculate the char index for easting/northing separator
            int remainder = length - i;

            if (remainder % 2 != 0) {
                //       doAToast("MGRS has to have an even number \nof resolution digits", 1);

                return mgrsValid = false;
            }
        }
        return mgrsValid;*/
    }

    // TODO: validate if symbol is entered
    public boolean validUTM(EditText northingIn, EditText eastingIn, EditText zoneNumberIn, EditText zoneLetterIn) {
        //    if (D) Log.i(debugTag, "validUTM()");

        boolean utmValid = true;

        String northingString = northingIn.getText().toString();
        String eastingString = eastingIn.getText().toString();
        String zoneNumberString = zoneNumberIn.getText().toString();
        String zoneLetterString = zoneLetterIn.getText().toString();

        if (northingString == null || northingString.length() == 0) {
            //         doAToast("Enter Northing", 1);

            return utmValid = false;
        }

        if (eastingString == null || eastingString.length() == 0) {
            //         doAToast("Enter Easting", 1);

            return utmValid = false;
        }

        if (zoneNumberString == null || zoneNumberString.length() == 0) {
            //        doAToast("Enter Zone Number", 1);

            return utmValid = false;
        }

        if (zoneLetterString == null || zoneNumberString.length() == 0) {
            //      doAToast("Enter Zone Letter", 1);

            return utmValid = false;
        }

        double northing = Double.parseDouble(northingString);
        double easting = Double.parseDouble(eastingString);
        int zoneNumber = (int) Float.parseFloat(zoneNumberString);
        char zoneLetter = zoneLetterString.charAt(0);             //converts the zoneLetter to a char
        zoneLetter = Character.toUpperCase(zoneLetter);

        if (northing < 0) {
            //        doAToast("Invalid Northing \n" + "Northing must be a positive number", 1);

            return utmValid = false;
        }

        if (easting < 0) {
            //         doAToast("Invalid Easting \n" + "Easting must be a positive number", 1);

            return utmValid = false;
        }

        if (zoneNumber < 1 || zoneNumber > 60) {
            //         doAToast("Invalid Zone Number \n" + "Zone number must be between 1 and 60", 1);

            return utmValid = false;
        }

        if (zoneLetter != 'N' && zoneLetter != 'S') {
            //          doAToast("Invalid Zone Letter \n" + "Zone Letter must be either N or S", 1);

            return utmValid = false;
        }

        return utmValid;
    }

    // checks for valid Lat/Lon
    // display error messages
    // TODO: validate if a symbol is entered
    public boolean validLatLon(EditText latIn, EditText lonIn) {
        //   if (D) Log.i(debugTag, "validLatLon()");

        boolean LatLonValid = true;

        String latString = latIn.getText().toString();
        String lonString = lonIn.getText().toString();

        if ((latString == null) || (latString.length() == 0)) {
            //       if (T) doAToast("Enter Latitude", 1);

            return LatLonValid = false;
        }

        if ((lonString == null) || (lonString.length() == 0)) {
            //       if (T) doAToast("Enter Longitude", 1);

            return LatLonValid = false;
        }

        double lat = Double.parseDouble(latString);
        double lon = Double.parseDouble(lonString);

        if ((lat > 90) || (lat < -90)) {
            //         doAToast("Invalid Latitude", 1);

            return LatLonValid = false;
        }

        if ((lon < -180) || (lon > 180)) {
            //        doAToast("Invalid Longitude", 1);

            return LatLonValid = false;
        }

        return LatLonValid;
    }
}
