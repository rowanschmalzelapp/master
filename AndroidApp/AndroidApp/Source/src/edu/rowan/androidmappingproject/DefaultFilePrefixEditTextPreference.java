package edu.rowan.androidmappingproject;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 7/15/13
 * <br>
 * <br>
 */
public class DefaultFilePrefixEditTextPreference extends EditTextPreference {
    EditText editText = null;

    public DefaultFilePrefixEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(R.layout.fileprefixedittextpreference);
    }

    @Override
    protected View onCreateDialogView() {
        return super.onCreateDialogView();
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        editText = (EditText) view.findViewById(R.id.filePrefixETPreferenceEditTextField);
        try {
            editText.setText(FirstActivity.returnInternalStorageInfo(Global.context.getString(R.string.offlineMapTilePrefixFN)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        FirstActivity.writeIntoInternalStorage(Global.context.getString(R.string.offlineMapTilePrefixFN), editText.getText().toString());

    }
}
