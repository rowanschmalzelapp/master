package edu.rowan.androidmappingproject;

import android.os.Environment;
import android.util.Log;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * David Galos
 * 6/29/13
 * <br>
 * <br>
 */
public class CustomTileProvider implements TileProvider {
    final String TAG = "CustomTileProvider";


    private static final int TILE_WIDTH = 256;
    private static final int TILE_HEIGHT = 256;
    private static final int BUFFER_SIZE = 16 * 1024;

    //private AssetManager assets;  //for embedded maps

    public CustomTileProvider() {
        //assets = Global.context.getAssets();  //for embedded maps
    }

    @Override
    public Tile getTile(int x, int y, int zoom) {
        byte[] image = readTileImage(x, y, zoom);
        return image == null ? null : new Tile(TILE_WIDTH, TILE_HEIGHT, image);
    }

    private byte[] readTileImage(int x, int y, int zoom) {
        InputStream in = null;
        ByteArrayOutputStream buffer = null;

        try {
            //in = assets.open(getTileFilename(x, y, zoom));     //open embedded map
            in = getTileFromSD(getTileFilename(x, y, zoom));     //open map from folder

            buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[BUFFER_SIZE];
            while ((nRead = in.read(data, 0, BUFFER_SIZE)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();

            return buffer.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        } finally {
            if (in != null) try {
                in.close();
            } catch (Exception ignored) {
            }
            if (buffer != null) try {
                buffer.close();
            } catch (Exception ignored) {
            }
        }
    }


    private String getTileFilename(int x, int y, int zoom) {

        // tilename = "sdcard0/Download/cyclemap/oc_" + x + "_" + y + "_" + zoom + ".png"; //from internal downloads dir
        // tilename= "sdcard1/cyclemap/oc_" + x + "_" + y + "_" + zoom + ".png"; //from sd card
        String filePath;

        // if(Environment.isExternalStorageRemovable()){
        try {
            filePath = FirstActivity.returnInternalStorageInfo(Global.context.getString(R.string.offlineMapPathFN)) + Integer.toString(zoom) + "/"
                    + FirstActivity.returnInternalStorageInfo(Global.context.getString(R.string.offlineMapTilePrefixFN))
                    + x + "_" + y + "_" + zoom + ".png";
        } catch (IOException e) {
            e.printStackTrace();
            filePath = "sdcard1/maps/" + Integer.toString(zoom) + "/oc_" + x + "_" + y + "_" + zoom + ".png";
        }
        //  }
        return filePath;
    }

    private InputStream getTileFromSD(String tilename) {
        InputStream inputStream = null;

        //this will return /storage/ on the Motorola Xoom. the SD card is /sdcard1 in that directory
        //must return the parent file of external storage directory or will return /storage/sdcard0 which is internal
        File filesDir = Environment.getExternalStorageDirectory().getParentFile();  //external sd

        filesDir.setReadable(true);

        File imageFile = null;
        if (filesDir != null) {
            imageFile = new File(filesDir, tilename);
        } else {
            Log.w(TAG, "DIRECTORY_DOWNLOADS is not available!");
        }

        FileInputStream fileInputStream = null;

        if (imageFile != null) {
            try {
                fileInputStream = new FileInputStream(imageFile);
            } catch (FileNotFoundException e) {
                //this thing is always pissed and makes teh log hard to read
                //e.printStackTrace();
            }

            inputStream = new BufferedInputStream(fileInputStream, BUFFER_SIZE);
            if (inputStream != null) {
                inputStream.mark(BUFFER_SIZE);
            }
        } else {
            Log.w(TAG, "PNG image " + tilename + " is not available!");
        }

        return inputStream;
    }


}
