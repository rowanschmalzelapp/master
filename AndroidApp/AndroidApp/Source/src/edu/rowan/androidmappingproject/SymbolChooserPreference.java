package edu.rowan.androidmappingproject;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 7/11/13
 * <br>
 * <br>
 */
public class SymbolChooserPreference extends DialogPreference {

    public static final FirstActivity.SYMBOLDICT[] defaultChoice = new FirstActivity.SYMBOLDICT[2];

    public SymbolChooserPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.symbolchooser);
    }

    @Override
    protected View onCreateDialogView() {

        try {
            Log.wtf("SymbolChooserPreference", "Current Default symbol = " + returnInternalStorageInfo(Global.context.getString(R.string.defaultSymbolFN)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.onCreateDialogView();


    }

    @Override
    protected void onBindDialogView(View d) {
        super.onBindDialogView(d);

        //top left image buttion in dialog
        final ImageButton ibChosenP = (ImageButton) d.findViewById(R.id.ib_chosensymbol);

        //16 imagebuttons representing the 16 standard symbols
        ImageButton ibFriendlyGroundP = (ImageButton) d.findViewById(R.id.ib_friend_ground);
        ImageButton ibFriendlySeaSurfaceP = (ImageButton) d.findViewById(R.id.ib_friend_seasurface);
        ImageButton ibFriendlyAirSpaceP = (ImageButton) d.findViewById(R.id.ib_friend_airspace);
        ImageButton ibFriendlySubsurfaceP = (ImageButton) d.findViewById(R.id.ib_friend_subsurface);
        ImageButton ibHostileGroundP = (ImageButton) d.findViewById(R.id.ib_hostile_ground);
        ImageButton ibHostileSeaSurfaceP = (ImageButton) d.findViewById(R.id.ib_hostile_seasurface);
        ImageButton ibHostileAirSpaceP = (ImageButton) d.findViewById(R.id.ib_hostile_airspace);
        ImageButton ibHostileSubsurfaceP = (ImageButton) d.findViewById(R.id.ib_hostile_subsurface);
        ImageButton ibNeutralGroundP = (ImageButton) d.findViewById(R.id.ib_neutral_ground);
        ImageButton ibNeutralSeaSurfaceP = (ImageButton) d.findViewById(R.id.ib_neutral_seasurface);
        ImageButton ibNeutralAirSpaceP = (ImageButton) d.findViewById(R.id.ib_neutral_airspace);
        ImageButton ibNeutralSubsurfaceP = (ImageButton) d.findViewById(R.id.ib_neutral_subsurface);
        ImageButton ibUnknownGroundP = (ImageButton) d.findViewById(R.id.ib_unknown_ground);
        ImageButton ibUnknownSeaSurfaceP = (ImageButton) d.findViewById(R.id.ib_unknown_seasurface);
        ImageButton ibUnknownAirSpaceP = (ImageButton) d.findViewById(R.id.ib_unknown_airspace);
        ImageButton ibUnknownSubsurfaceP = (ImageButton) d.findViewById(R.id.ib_unknown_subsurface);

        //lookup which is hte default and set it.
        try {
            ibChosenP.setImageResource(returnSymbolFromString(returnInternalStorageInfo(Global.context.getString(R.string.defaultSymbolFN))).drawable);
        } catch (IOException e) {
            ibChosenP.setImageResource(FirstActivity.SYMBOLDICT.FriendlyGround.drawable);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        //set up all the listeners for the buttons
        //when one of these is selected the top left image will change to the tapped image
        //currentChoice[0] will be updated with the new selection.
        ibFriendlyGroundP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.friend_ground);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.FriendlyGround;
            }
        });

        ibFriendlySeaSurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.friend_seasurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.FriendlySeaSurface;
            }
        });

        ibFriendlyAirSpaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.friend_airspace);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.FriendlyAirSpace;

            }
        });

        ibFriendlySubsurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.friend_subsurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.FriendlySubsurface;

            }
        });

        ibHostileGroundP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.hostile_ground);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.HostileGround;

            }
        });

        ibHostileSeaSurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.hostile_seasurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.HostileSeaSurface;

            }
        });

        ibHostileAirSpaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.hostile_airspace);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.HostileAirSpace;

            }
        });

        ibHostileSubsurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.hostile_subsurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.HostileSubsurface;

            }
        });

        ibNeutralGroundP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.neutral_ground);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.NeutralGround;

            }
        });

        ibNeutralSeaSurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.neutral_seasurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.NeutralSeaSurface;

            }
        });

        ibNeutralAirSpaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.neutral_airspace);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.NeutralAirSpace;

            }
        });

        ibNeutralSubsurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.neutral_subsurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.NeutralSubsurface;

            }
        });

        ibUnknownGroundP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.unknown_ground);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.UnknownGround;

            }
        });

        ibUnknownSeaSurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.unknown_seasurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.UnknownSeaSurface;

            }
        });

        ibUnknownAirSpaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.unknown_airspace);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.UnknownAirSpace;

            }
        });

        ibUnknownSubsurfaceP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ibChosenP.setImageResource(R.drawable.unknown_subsurface);
                defaultChoice[0] = FirstActivity.SYMBOLDICT.UnknownSubsurface;

            }
        });

    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        //set the key to the top left choice
        //defaultChoice[0]

        if (defaultChoice[0] == null) {
            defaultChoice[0] = FirstActivity.SYMBOLDICT.NeutralGround;
        }
        Log.wtf("SymbolChooserPreference", "Selected symbol = " + defaultChoice[0].toString());

        writeIntoInternalStorage(Global.context.getString(R.string.defaultSymbolFN), defaultChoice[0].toString());

        FirstActivity.ibSidebarMain.setImageResource(defaultChoice[0].drawable);
        FirstActivity.currentChoice[0] = defaultChoice[0];
        super.onDialogClosed(positiveResult);

    }


    @SuppressWarnings("ConstantConditions")
    public FirstActivity.SYMBOLDICT returnSymbolFromString(String inputString) {
        FirstActivity.SYMBOLDICT symbol = null;
        Log.wtf("SymbolChooserPreference", "RTNSymFromStr input = " + inputString);
        if (inputString.matches(FirstActivity.SYMBOLDICT.FriendlyAirSpace.toString())) {
            symbol = FirstActivity.SYMBOLDICT.FriendlyAirSpace;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.FriendlyGround.toString())) {
            symbol = FirstActivity.SYMBOLDICT.FriendlyGround;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.FriendlySeaSurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.FriendlySeaSurface;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.FriendlySubsurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.FriendlySubsurface;
        }

        if (inputString.matches(FirstActivity.SYMBOLDICT.HostileAirSpace.toString())) {
            symbol = FirstActivity.SYMBOLDICT.HostileAirSpace;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.HostileGround.toString())) {
            symbol = FirstActivity.SYMBOLDICT.HostileGround;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.HostileSeaSurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.HostileSeaSurface;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.HostileSubsurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.HostileSubsurface;
        }

        if (inputString.matches(FirstActivity.SYMBOLDICT.NeutralAirSpace.toString())) {
            symbol = FirstActivity.SYMBOLDICT.NeutralAirSpace;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.NeutralGround.toString())) {
            symbol = FirstActivity.SYMBOLDICT.NeutralGround;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.NeutralSeaSurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.NeutralSeaSurface;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.NeutralSubsurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.NeutralSubsurface;
        }

        if (inputString.matches(FirstActivity.SYMBOLDICT.UnknownAirSpace.toString())) {
            symbol = FirstActivity.SYMBOLDICT.UnknownAirSpace;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.UnknownGround.toString())) {
            symbol = FirstActivity.SYMBOLDICT.UnknownGround;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.UnknownSeaSurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.UnknownSeaSurface;
        }
        if (inputString.matches(FirstActivity.SYMBOLDICT.UnknownSubsurface.toString())) {
            symbol = FirstActivity.SYMBOLDICT.UnknownSubsurface;
        }

        Log.wtf("SymbolChooserPreference", "RTNSymFromStr output = " + symbol.toString());
        return symbol;
    }


    private void writeIntoInternalStorage(String FILENAME, String value) {
        FileOutputStream fos;
        try {
            fos = Global.context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            fos.write(value.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String returnInternalStorageInfo(String FILENAME) throws IOException {

        FileInputStream fis;
        String data = null;
        try {
            fis = Global.context.openFileInput(FILENAME);
            InputStreamReader in = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(in);
            data = br.readLine();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}
