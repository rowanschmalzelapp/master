package edu.rowan.androidmappingproject;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 4/4/13
 * <br>
 * <br>
 */
public class LatLngDMS {
    private float degreesLon = 0;
    private float minutesLon = 0;
    private float secondsLon = 0;

    public float getDegreesLat() {
        return degreesLat;
    }

    public void setDegreesLat(float degreesLat) {
        this.degreesLat = degreesLat;
    }

    public float getMinutesLat() {
        return minutesLat;
    }

    public void setMinutesLat(float minutesLat) {
        this.minutesLat = minutesLat;
    }

    public float getSecondsLat() {
        return secondsLat;
    }

    public void setSecondsLat(float secondsLat) {
        this.secondsLat = secondsLat;
    }

    private float degreesLat = 0;
    private float minutesLat = 0;
    private float secondsLat = 0;

    public float getDegreesLon() {
        return degreesLon;
    }

    public void setDegreesLon(float degrees) {
        this.degreesLon = degrees;
    }

    public float getMinutesLon() {
        return minutesLon;
    }

    public void setMinutesLon(float minutes) {
        this.minutesLon = minutes;
    }

    public float getSecondsLon() {
        return secondsLon;
    }

    public void setSecondsLon(float seconds) {
        this.secondsLon = seconds;
    }

    @Override
    public String toString() {

        String latDMS = "";
        latDMS = degreesLat + "* " + minutesLat + "\' " + secondsLat + "\"";

        String lonDMS = "";
        lonDMS = degreesLon + "* " + minutesLon + "\' " + secondsLon + "\"";

        String DMS = latDMS + ", " + lonDMS;
        return DMS;
    }
}
