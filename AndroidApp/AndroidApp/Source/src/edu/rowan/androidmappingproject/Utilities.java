package edu.rowan.androidmappingproject;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.widget.EditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 4/4/13
 * <br>
 * <br>
 */
public class Utilities {
    static SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(Global.context);

    /**
     * This function will create a time stamp from the current system clock and return it as a string
     *
     * @return String current system time in 24 hour military format <p>hhmm:ss dd mmm yyyy</p>
     */
    public static String getTimeStamp() {
        //1=HH:MM:SS  2=HHMM:SS   3=HHMMSS
        int tsFormat = Integer.parseInt(sharedPreferences.getString("prefTimestampFormat", "1"));

        Time now = new Time();
        now.setToNow();
        int day = now.monthDay;
        int hour = now.hour;
        int min = now.minute;
        int year = now.year;
        int sec = now.second;
        int month = now.month;

        StringBuilder milTime = new StringBuilder();

        if (hour > 9) {
            milTime.append(hour);
        } else {
            milTime.append("0" + hour);
        }

        if (tsFormat == 1) {
            milTime.append(":");
        }

        if (min > 9) {
            milTime.append(min);
        } else {
            milTime.append("0" + min);
        }

        if (tsFormat == 1 || tsFormat == 2) {
            milTime.append(":");
        }

        if (sec > 9) {
            milTime.append(sec);
        } else {
            milTime.append("0" + sec);
        }


        milTime.append(" " + day + " ");
        switch (month) {
            case 0:
                milTime.append("JAN");
                break;
            case 1:
                milTime.append("FEB");
                break;
            case 2:
                milTime.append("MAR");
                break;
            case 3:
                milTime.append("APR");
                break;
            case 4:
                milTime.append("MAY");
                break;
            case 5:
                milTime.append("JUN");
                break;
            case 6:
                milTime.append("JUL");
                break;
            case 7:
                milTime.append("AUG");
                break;
            case 8:
                milTime.append("SEP");
                break;
            case 9:
                milTime.append("OCT");
                break;
            case 10:
                milTime.append("NOV");
                break;
            case 11:
                milTime.append("DEC");
                break;
        }
        milTime.append(" ");
        milTime.append(year);

        return milTime.toString();
    }

    /**
     * Check to see if an EditText field is empty
     *
     * @param etText an EditText field
     * @return boolean true if the field is empty, false otherwise
     */
    public boolean checkEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return true;
        else
            return false;
    }

    /**
     * copy file from source to destination
     *
     * @param src source
     * @param dst destination
     * @throws java.io.IOException in case of any problems
     */
    public static void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }
    }
}
