package edu.rowan.androidmappingproject;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceManager;

/**
 * Created with IntelliJ IDEA.
 * Justin Reda
 * 6/30/13
 * <br>
 * <br>
 */
public class SettingsActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out); //animation to make it look nice

        PreferenceManager.setDefaultValues(getBaseContext(), R.xml.settings, false);

        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new SettingsFragment()).commit();

    }

    @Override
    public void onBackPressed() {
        finish();//go back to the FirstActivity
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out); //animation to make it look nice
    }
}