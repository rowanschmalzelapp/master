package com.example;

import java.util.Scanner;

/*
This program is designed to be a calculator between two times that also include dates.
Keep in mind the number of days in each month, leap years, and what times are actually valid (assume military time).
Make sure time 1 is earlier then time 2 then make it work even if it isn't.
You may assume the user adheres to the correct input format, but do not assume always valid input numbers.
Do not worry about having the program repeat, it can end after calculating the time difference and returning to user.

This problem is to test substrings (string parsing), data type conversions, and user input loops.

For an extra challenge, try using the Java "Date" class to pull the system time for one of the inputs.
We will be using a lot of classes like this in the android apps so starting here would be a good idea.
http://www.tutorialspoint.com/java/java_date_time.htm
 */

public class Main {

    public static void main(String[] args) {

        boolean timeOneValid=false, timeTwoValid=false;

        Scanner scanner = new Scanner(System.in);

        //format = hour:minute_month.day.year
        //enter first time (military format)
        System.out.println("Enter First Time in format hhmm_mm.dd.yyyy");
        String timeOne=scanner.next();

        //Parse each string individually into "substrings" of time components
        //start with timeOne. *hint try typing "timeOne." and look at available commands.
        //see http://docs.oracle.com/javase/7/docs/api/java/lang/String.html for more info on commands.
        String hourOneString="";//so you want hh. position 0 to 2 (big hint)
        String minOneString="";
        String monthOneString="";
        String dayOneString="";
        String yearOneString="";

        //Convert each string into a more appropriate data type (ints for example)
        //See http://docs.oracle.com/javase/7/docs/api/java/lang/Integer.html for commands that may be useful.
        int hourOneInt=0;


        //Check to make sure each of the inputs are valid, and make the user re-enter data if something is wrong.
        /*Hint: a while loop starting before "Enter first time..." and ending here (after verification)
        would be the way to go. The boolean timeOneValid should be used for this.*/
        //is hour between 00 and 23?
        //is minute between 00 and 59?
        //is month between 00 and 12?
        //is day appropriate for the given month/year?
        //is year valid (you can cut it off at 1920 and 2050)



        //repeat for timeTwo
        //enter second time
        System.out.println("Enter Second Time in format hhmm_mm.dd.yyyy");
        String timeTwo=scanner.next();

        //parse strings

        //convert to more usable data type

        //make sure inputs are valid




        //decide which time/data is chronologically first
        //second time minus first time equals time difference

        System.out.println("The time difference is: ");
    }
}